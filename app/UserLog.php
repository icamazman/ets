<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    use HasFactory;

    protected $table = 'user_log';

    protected $fillable = [
        'user_id',
        'attempt',
        'user_created',
        'user_lastmaintain'
    ];

    public function users_log()
    {
        return $this->hasOne(User::class, 'user_id', 'id');
    }

    public function attempt_param()
    {
        return $this->hasOne(Parameter::class, 'attempt', 'param_code');
    }
}
