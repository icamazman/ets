<?php

namespace App\Exports;

use App\Ticket_IT;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class TicketExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Ticket_IT::with([
            'users',
            'req_users',
            'departments',
            'req_departments',
            'request_types',
            'requirement_types',
            'applications',
            'criticalities',
            'status',
            'assignees',
            'officers',
            'complexities',
            'requirement_types_it',
            'owners',
        ])->get();
    }

    public function map($ticket_it): array
    {
        return [
            $ticket_it->id,
            $ticket_it->urf_no,
            $ticket_it->users->fullname,
            $ticket_it->departments->param_desc,
            $ticket_it->user_email,
            $ticket_it->user_ext,
            $ticket_it->requestor_fullname == 0 ? '' : $ticket_it->req_users->fullname,
            $ticket_it->requestor_fullname == 0 ? '' : $ticket_it->req_departments->param_desc,
            $ticket_it->requestor_fullname == 0 ? '' : $ticket_it->requestor_email,
            $ticket_it->requestor_fullname == 0 ? '' : $ticket_it->requestor_ext,
            $ticket_it->request_types->param_desc,
            $ticket_it->requirement_types->param_desc,
            $ticket_it->request_type == "rq01" ? $ticket_it->applications->param_desc : '',
            $ticket_it->others,
            $ticket_it->description,
            $ticket_it->criticalities->param_desc,
            $ticket_it->required_completion_date,
            $ticket_it->required_owner_approval,
            $ticket_it->status->param_desc,
            $ticket_it->user_created,
            $ticket_it->created_at,
            $ticket_it->updated_at,
            $ticket_it->user_lastmaintain,
            $ticket_it->assignTo == true ? $ticket_it->assignees->param_desc : '',
            $ticket_it->dtassign,
            $ticket_it->officer == true ? $ticket_it->officers->fullname : '',
            $ticket_it->requirement_type_it == true ? $ticket_it->requirement_types_it->param_desc : '',
            $ticket_it->complexity == true ? $ticket_it->complexities->param_desc : '',
            $ticket_it->dt_ictd_testing,
            $ticket_it->dt_user_testing,
            $ticket_it->others_it,
        ];
    }

    public function headings(): array
    {
        return[
            'ID',
            'URF_NO',
            'USER FULLNAME',
            'USER DEPARTMENT',
            'USER EMAIL',
            'USER EXT PHONE NO',
            'REQUESTOR FULLNAME',
            'REQUESTOR DEPARTMENT',
            'REQUESTOR EMAIL',
            'REQUESTOR EXT PHONE NO',
            'REQUEST TYPE',
            'REQUIREMENTM TYPE',
            'APPLICATION',
            'OTHERS',
            'DESCRIPTION',
            'CRITICALITY',
            'REQUIRED COMPLETION DATE',
            'REQUIRED OWNER APPROVAL',
            'URF STATUS',
            'CREATED BY',
            'CREATED ON',
            'UPDATED ON',
            'UPDATED BY',
            'ASSIGN TO',
            'ASSIGNATION DATE',
            'OFFICER IN-CHARGE',
            'REQUIREMENT TYPE (ICTD)',
            'COMPLEXITY',
            'ICTD TESTING DATE',
            'USER TESTING DATE',
            'OTHERS (ICTD)'
        ];
    }
}
