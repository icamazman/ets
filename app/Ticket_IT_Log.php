<?php

namespace App;

use App\Http\Traits\Hashidable;
use Attribute;
use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Database\Eloquent\Model;

class Ticket_IT_Log extends Model
{
    // use Hashidable;

    // protected $appends = ['hashed_id'];
    protected $table='ticket_it_log';
    protected $fillable = [
            'urf_no',
            'urf_status',
            'remark',
            'user_created',
            'user_lastmaintain',
    ];

    public function status()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'urf_status');
    }

    public function tickets()
    {
        return $this->hasMany(Ticket_IT::class, 'id', 'urf_no');
    }

    // public function getHashedIdAttribute()
    // {
    //     return Hashids::connection(get_called_class())->encode($this->getKey());
    // }


}
