<?php

namespace App\Http\Controllers;

use App\User;
use App\UserLog;
use App\Parameter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class SecureProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with(['departments', 'units', 'branches', 'user_status', 'roles_param', 'roles'])->find(Auth::id());

        $unit = $this->getParameter('unit');
        $branch = $this->getParameter('branch');
        $department = $this->getParameter('department');
        $roles = $this->getRole();
        // dd($users);

        return view('security.secure_profile.index', compact('users', 'unit', 'branch', 'department', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->has('changepass')){
            $validatepass = $request->validate([
                'password' => 'required|min:8|confirmed',
            ]);

            UserLog::create([
                'user_id' => Auth::id(),
                'attempt' => 'att05',
                'user_created' => Auth::user()->fullname,
                'user_lastmaintain' => Auth::user()->fullname,
            ]);

            $changepassword = User::where('id', $id)->update(['password' => bcrypt($request->password)]);
        }
        // dd($request->has('changeava'));
        if($request->has('changeava')){
            // dd($request->file('avatar_path'));
            if($request->file('avatar_path')){
                $fileName = Carbon::now()->format('Y-m-d').'_'.$request->file('avatar_path')->getClientOriginalName();
                $filePath = $request->file('avatar_path')->storeAs('storage/avatars', $fileName, 'public');
                $img = Image::make($request->file('avatar_path')->getRealPath());
                // $img->resizeCanvas(1500, 1500, 'center');
                // $img->greyscale();
                $img->save(public_path('images/faces/'.$fileName));
                // dd($img);
            }

            UserLog::create([
                'user_id' => Auth::id(),
                'attempt' => 'att01',
                'user_created' => Auth::user()->fullname,
                'user_lastmaintain' => Auth::user()->fullname,
            ]);

            User::where('id', $id)->update([
                'avatar_name' => $fileName,
                'avatar_path' => $filePath
            ]);
        }

        if($request->has('changedept')){
            UserLog::create([
                'user_id' => Auth::id(),
                'attempt' => 'att02',
                'user_created' => Auth::user()->fullname,
                'user_lastmaintain' => Auth::user()->fullname,
            ]);

            User::where('id', $id)->update([
                'department' => $request->department,
            ]);
        }

        if($request->has('changeunit')){
            UserLog::create([
                'user_id' => Auth::id(),
                'attempt' => 'att03',
                'user_created' => Auth::user()->fullname,
                'user_lastmaintain' => Auth::user()->fullname,
            ]);

            User::where('id', $id)->update([
                'unit' => $request->unit,
            ]);
        }

        if($request->has('changebranch')){
            UserLog::create([
                'user_id' => Auth::id(),
                'attempt' => 'att04',
                'user_created' => Auth::user()->fullname,
                'user_lastmaintain' => Auth::user()->fullname,
            ]);

            User::where('id', $id)->update([
                'branch' => $request->branch,
            ]);
        }

        return redirect()->route('secure_profile.index')->with('msg', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getParameter($parameter_name){
        return Parameter::select('param_code', 'param_desc')
        ->where('category_code', $parameter_name)
        ->orderBy('param_desc')
        ->pluck('param_desc', 'param_code');
    }

    public function getRole(){
        return Role::select('id', 'name')
        ->orderBy('id')
        ->pluck('name', 'id');
    }
}
