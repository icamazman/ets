<?php

namespace App\Http\Controllers;

use App\User;
use App\Parameter;
use App\Ticket_IT;
use Carbon\Carbon;
use App\Mail\URFSent;
use App\Ticket_IT_Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;

class ICTAssignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // $data = Student::latest()->get();
            $data = Ticket_IT::where('urf_status', 'stat06')->orWhere('urf_status', 'stat09')
            ->where(function ($q){
                if(Auth::user()->username == 'hanita'){
                    $q->where('request_type', 'rq01');
                }
                if(Auth::user()->username == 'budi'){
                    $q->where('request_type', 'rq02');
                }
                if(Auth::user()->username == 'hanita' || Auth::user()->username == 'siti' || Auth::user()->username == 'budi' ){
                    $q->orWhere('request_type', 'rq03');
                }
            })
            ->with(['users', 'req_users', 'departments', 'req_departments', 'request_types', 'requirement_types', 'applications', 'criticalities','status', 'officers', 'requirement_types_it', 'complexities', 'owners'])
            ->distinct()
            ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('urf_no', function($row){
                    // if( in_array($row->urf_status, array('stat01', 'stat02', 'stat03', 'stat05', 'stat08', 'stat13')) )
                    // {
                        return "URF".sprintf('%05d', $row->id);
                    // }else
                    //     return "ETS".sprintf('%05d', $row->urf_no);
                })
                ->addColumn('fullname', function($row){
                    return $row->users->fullname;
                })
                ->addColumn('mergeColumn', function($row){
                    if($row->request_type == "rq01" || $row->request_type == "Application"){
                        return $row->request_types->param_desc." - ".$row->applications->param_desc;
                    }
                    else{
                        return $row->request_types->param_desc;
                    }
                })
                ->addColumn('urf_status', function($row){
                    if($row->urf_status == "stat12"){
                        if($row->assignTo == "ass01"){
                            return $row->status->param_desc." - Assigned to ".$row->officers->fullname;
                        }else{
                            return $row->status->param_desc." - Assigned to Vendor";
                        }
                    }else{
                        return $row->status->param_desc;
                    }
                })
                ->addColumn('created_at', function($row){
                    return $row->created_at ? with(new Carbon($row->created_at))->format('d/m/Y h:m') : '';
                })
                ->addColumn('updated_at', function($row){
                    return $row->updated_at ? with(new Carbon($row->updated_at))->format('d/m/Y h:m') : '';
                })
                ->addColumn('action', function($row){
                    // $actionBtn = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm">Delete</a>';
                    $actionBtn = '<a href="'. route('ict_assign.edit', $row->id ) .'"><button class="btn btn-primary btn-sm"><i class="mdi mdi-magnify"></i></button></button></a></td>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }



        return view('ict_workstation.ict_assign.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        $tracking = Ticket_IT::where('id','=', $id)
        ->with(['users', 'req_users', 'departments', 'req_departments', 'request_types', 'requirement_types', 'applications', 'criticalities','status', 'officers', 'requirement_types_it', 'complexities', 'owners', 'files'])
        ->distinct()
        ->first();


        if ($request->ajax()) {
            // $data = Student::latest()->get();
            $data = Ticket_IT_Log::with('status', 'tickets')
            ->where('urf_no', '=', $id)
            ->distinct()
            ->get();


            // dd($data);

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('urf_no', function($row){
                    return "URF".sprintf('%05d', $row->urf_no);
                })
                ->addColumn('urf_status', function($row){
                    return $row->status->param_desc;
                })
                ->editColumn('updated_at', function ($row) {
                    return $row->updated_at ? with(new Carbon($row->updated_at))->format('d/m/Y h:m') : '';;
                })
                ->make(true);
        }

        $assignee = $this->getParameter('assignee');
        $officer = $this->getOfficer('dept05');

        return view('ict_workstation.ict_assign.edit', compact('tracking', 'assignee', 'officer') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $assigning = Ticket_IT::where('id', $id)
        ->update([
            'urf_status' => $request->urf_status,
            'assignTo' => $request->assignTo,
            'dtassign' => $request->dtassign,
            'officer' => $request->officer,
            'user_lastmaintain' => $request->user_lastmaintain
        ]);

        $insert_log = Ticket_IT_Log::create($request->all());

        $app = User::where('department', 'dept05')->where('username', 'hanita')->orWhere('username', 'rafidah')->orWhere('username', 'fairoz')->select('email')->pluck('email');
        $hdwr = User::where('department', 'dept05')->where('username', 'budi')->orWhere('username', 'nizam')->select('email')->pluck('email');
        $oth = User::where('department', 'dept05')->where('username', 'hanita')->orWhere('username', 'rafidah')->orWhere('username', 'fairoz')->orWhere('username', 'nizam')->select('email')->pluck('email');

        $ticket = Ticket_IT::where('id', $id)->first();

        if($request->assignTo == "ass01")
        {
            $Officer = User::with('officers')->where('id', '=', $request->officer);
            $OfficerEmail = $Officer->first()->email;
            $OfficerName = $Officer->first()->fullname;

            if($ticket->request_type == "rq01"){
                Mail::to($OfficerEmail) /* HOU */
                ->cc($app)
                // ->bcc($request->user_email /* ets-noreply */)
                ->send(new URFSent($id));
            }

            if($ticket->request_type == "rq02"){
                Mail::to($OfficerEmail) /* HOU */
                ->cc($hdwr)
                // ->bcc($request->user_email /* ets-noreply */)
                ->send(new URFSent($id));
            }

            if($ticket->request_type == "rq03"){
                Mail::to($OfficerEmail) /* HOU */
                ->cc($oth)
                // ->bcc($request->user_email /* ets-noreply */)
                ->send(new URFSent($id));
            }

            $ICTClerkName = null;

        }

        if($request->assignTo == "ass02")
        {
            $ICTClerk = User::where('department', '=', 'dept05')->where('fullname', 'Nur Syafiqah Taib');
            $ICTClerkName = $ICTClerk->first()->fullname;
            $ICTClerkEmail = $ICTClerk->first()->email;

            Mail::to($ICTClerkEmail) /* HOU */
            // ->bcc($request->user_email /* ets-noreply */)
            ->send(new URFSent($id));

            $OfficerName = null;
        }

        return redirect()->route('ict_assign.index')->with('msg', 'success')->with('assignee', $request->assignTo)->with('officer', $OfficerName)->with('vendor', $ICTClerkName);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getParameter($parameter_name){
        return Parameter::select('param_code', 'param_desc')
        ->where('category_code', $parameter_name)
        ->orderBy('param_desc')
        ->pluck('param_desc', 'param_code');
    }

    public function getOfficer($department){
        return User::select('id', 'fullname')
        ->where('department', $department)
        ->orderBy('fullname')
        ->pluck('fullname', 'id');
    }
}
