<?php

namespace App\Http\Controllers;

use App\User;
use App\Parameter;
use App\Ticket_IT;
use Carbon\Carbon;
use App\Ticket_IT_Log;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;


class MaintainURFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        if ($request->ajax()) {
            // $data = Student::latest()->get();
            $data = Ticket_IT::with(['users', 'req_users', 'departments', 'req_departments', 'request_types', 'requirement_types', 'applications', 'criticalities','status', 'officers', 'requirement_types_it', 'complexities', 'owners'])
            ->where('urf_status', '!=' ,'stat11')
            ->distinct()
            ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('urf_no', function($row){
                    // if( in_array($row->urf_status, array('stat01', 'stat02', 'stat03', 'stat05', 'stat08', 'stat13')) )
                    // {
                        return "URF".sprintf('%05d', $row->id);
                    // }else
                    //     return "ETS".sprintf('%05d', $row->urf_no);
                })
                ->addColumn('fullname', function($row){
                    return $row->users->fullname;
                })
                ->addColumn('mergeColumn', function($row){
                    if($row->request_type == "rq01"){
                        return $row->request_types->param_desc." - ".$row->applications->param_desc;
                    }
                    else if($row->request_type == "rq02" || $row->request_type == "rq03"){
                        return $row->request_types->param_desc;
                    }
                    else{
                        return null;
                    }
                })
                ->addColumn('urf_status', function($row){
                    if($row->urf_status == "stat12"){
                        if($row->assignTo == "ass01"){
                            return $row->status->param_desc." - Assigned to ".$row->officers->fullname;
                        }else{
                            return $row->status->param_desc." - Assigned to Vendor";
                        }
                    }else{
                        return $row->status->param_desc;
                    }
                })
                ->addColumn('action', function($row){
                    $hash = new Hashids();
                    // $actionBtn = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm">Delete</a>';
                    $actionBtn = '<a href="'. route('maintain_urf.edit', $hash->encodeHex($row->id) ) .'"><button class="btn btn-primary btn-sm"><i class="mdi mdi-magnify"></i></button></button></a></td>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('maintenance.urf.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $hash = new Hashids();

        $users = User::query()
        ->when($request->requestor_fullname, function($query, $requestor_id){
            return $query->where('id', $requestor_id);
        });

        if ($request->ajax() && $request->requestor_fullname) {
            $department = $users->first()->departments->param_desc;
            $users = $users->first();
            return ['department' => $department, 'users' => $users];
        }

        $users = $users->get();

        $tracking = Ticket_IT::where('id','=', $hash->decodeHex($id))
        ->with(['users', 'req_users', 'departments', 'req_departments', 'request_types', 'requirement_types', 'applications', 'criticalities','status', 'officers', 'requirement_types_it', 'complexities', 'owners', 'files'])
        ->distinct()
        ->first();

        if ($request->ajax()) {
            // $data = Student::latest()->get();
            $data = Ticket_IT_Log::with('status', 'tickets')
            ->where('urf_no', '=', $hash->decodeHex($id))
            ->distinct()
            ->get();

            // dd($data);

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('urf_no', function($row){
                    return "URF".sprintf('%05d', $row->urf_no);
                })
                ->addColumn('urf_status', function($row){
                    return $row->status->param_desc;
                })
                ->editColumn('updated_at', function ($row) {
                    return $row->updated_at ? with(new Carbon($row->updated_at))->format('d/m/Y h:m') : '';;
                })
                ->make(true);
        }

        $applications = $this->getParameter('applications');
        $request_type = $this->getParameter('request_type');
        $requirement_type = $this->getParameter('requirement_type');
        $critically = $this->getParameter('critically')->sortKeys();
        $department = $this->getParameter('department');
        $requirement_type_it = $this->getParameter('requirement_type_it');
        $complexity = $this->getParameter('complexity')->sortKeys();

        return view('maintenance.urf.edit', compact('tracking', 'users', 'applications', 'request_type', 'requirement_type', 'critically', 'department', 'requirement_type_it', 'complexity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getParameter($parameter_name){
        return Parameter::select('param_code', 'param_desc')
        ->where('category_code', $parameter_name)
        ->orderBy('param_desc')
        ->pluck('param_desc', 'param_code');
    }
}
