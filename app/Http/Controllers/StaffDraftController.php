<?php

namespace App\Http\Controllers;

use App\Ticket_IT;
use Carbon\Carbon;
use App\FileStorage;
use App\Ticket_IT_Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class StaffDraftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // $data = Student::latest()->get();
            $data = Ticket_IT::where(function($q){ $q->where('urf_status', '=', 'stat11')->where('user_fullname', '=', Auth::user()->id); })
            ->with(['users', 'req_users', 'departments', 'req_departments', 'request_types', 'requirement_types', 'applications', 'criticalities','status'])
            ->distinct()
            ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('urf_no', function($row){
                    // if( in_array($row->urf_status, array('stat01', 'stat02', 'stat03', 'stat05', 'stat08', 'stat13', 'stat11')) )
                    // {
                        return "URF".sprintf('%05d', $row->id);
                    // }else
                    //     return "ETS".sprintf('%05d', $row->urf_no);
                })
                ->addColumn('fullname', function($row){
                    return $row->users->fullname;
                })
                ->addColumn('mergeColumn', function($row){
                    if($row->request_type == "rq01"){
                        return $row->request_types->param_desc." - ".$row->applications->param_desc;
                    }
                    else if($row->request_type == "rq02" || $row->request_type == "rq03"){
                        return $row->request_types->param_desc;
                    }
                    else{
                        return null;
                    }
                })
                ->addColumn('created_at', function($row){
                    return $row->created_at ? with(new Carbon($row->created_at))->format('d/m/Y h:m') : '';
                })
                ->addColumn('action', function($row){
                    // $actionBtn = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm">Delete</a>';
                    $actionBtn = '<a href="'. route('staff_draft.edit', $row->id ) .'"><button class="btn btn-primary btn-sm"><i class="mdi mdi-magnify"></i></button></button></a></td>';
                    // $actionBtn = '';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('staff.staff_draft.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tracking = Ticket_IT::where('id','=', $id)
        ->with(['users', 'req_users', 'departments', 'req_departments', 'request_types', 'requirement_types', 'applications', 'criticalities','status', 'files'])
        ->distinct()
        ->first();

        return view('staff.staff_draft.edit', compact('tracking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Ticket_IT::where('id', $id)
        ->update([
            'request_type' => $request->request_type,
            'requirement_type' => $request->requirement_type,
            'application' => $request->application,
            'others' => $request->others,
            'description' => $request->description,
            'criticality' => $request->criticality,
            'required_completion_date' => $request->required_completion_date,
            'urf_status' => $request->urf_status,
            'user_updated' => $request->user_lastmaintain,
        ]);

        if($request->file())
        {
            $fileName = Carbon::now()->format('Y-m-d').'_'.time().'_'.$request->file('file_path')->getClientOriginalName();
            $filePath = $request->file('file_path')->storeAs('uploads', $fileName, 'public');
            $file = FileStorage::create([
                'name' => $fileName,
                'file_path' => $filePath,
                'user_created' => $request->user_created,
                'user_updated' => $request->user_lastmaintain,
                'ticket_id' => $id,
            ]);
        }

        $insert_log = Ticket_IT_Log::create($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
