<?php

namespace App\Http\Controllers;

use App\User;
use App\Parameter;
use App\Ticket_IT;
use App\FileStorage;
use App\Mail\URFSent;
use App\Ticket_IT_Log;
use Carbon\Carbon;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class URFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::query()
        ->when($request->requestor_fullname, function($query, $requestor_id){
            return $query->where('id', $requestor_id);
        });

        if ($request->ajax() && $request->requestor_fullname) {
            $department = $users->first()->departments->param_desc;
            $users = $users->first();
            return ['department' => $department, 'users' => $users];
        }

        $users = $users->orderBy('fullname')->get();

        $verifier = User::query()
        ->when($request->sendto, function($query, $sendto_id){
            return $query->where('id', $sendto_id);
        });

        if ($request->ajax() && $request->sendtoHOU) {
            $verifier = $verifier->first();
            return ['verifier' => $verifier];
        }

        $verifier = $verifier->orderBy('fullname')->get();

        $approver = User::query()
        ->when($request->sendto, function($query, $sendto_id){
            return $query->where('id', $sendto_id);
        });

        if ($request->ajax() && $request->sendtoHOD) {
            $approver = $approver->first();
            return ['approver' => $approver];
        }

        $approver = $approver->orderBy('fullname')->get();


        // dd(Auth::user()->parameters);

        $applications = $this->getParameter('applications');
        $request_type = $this->getParameter('request_type');
        $requirement_type = $this->getParameter('requirement_type');
        $critically = $this->getParameter('critically')->sortKeys();
        $department = $this->getParameter('department');

        return view('staff.form_urf', compact('users', 'applications', 'request_type', 'requirement_type', 'critically', 'department', 'verifier', 'approver'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->urf_status == "stat11"){
            $user_request = Ticket_IT::create($request->all());

            if($request->file())
            {
                $fileName = Carbon::now()->format('Y-m-d').'_'.time().'_'.$request->file('file_path')->getClientOriginalName();
                $filePath = $request->file('file_path')->storeAs('uploads', $fileName, 'public');
                FileStorage::create([
                    'name' => $fileName,
                    'file_path' => $filePath,
                    'user_created' => $request->user_created,
                    'user_updated' => $request->user_lastmaintain,
                    'ticket_id' => $user_request->id,
                ]);
            }

            $insert_log = Ticket_IT_Log::create([
                'urf_no' => $user_request->id,
                'urf_status' => $request->urf_status,
                'user_created' => $request->user_created,
                'user_lastmaintain' => $request->user_lastmaintain,
            ]);

            if($user_request->requestor_fullname){
                $mail = Mail::to($user_request->user_email, $user_request->requestor_email);
                /* Requestor & Requestor On Behalf */
            }else{
                $mail = Mail::to($user_request->user_email);
            }
            // ->bcc($request->user_email /* ets-noreply */)
            $mail->send(new URFSent($user_request->id));
        }


        $validated = $request->validate([
            "user_fullname" => "required",
            "user_dept" => "required",
            "user_email" => "required",
            "user_ext" => "required",
            "requestor_fullname" => "required_if:requestonbehalf,selected",
            "requestor_dept" => "required_if:requestonbehalf,selected",
            "requestor_email" => "required_if:requestonbehalf,selected",
            "requestor_ext" => "required_if:requestonbehalf,selected",
            "request_type" => "required",
            "requirement_type" => "required",
            "required_owner_approval" => "required_if:ownerapproval,selected",
            "required_completion_date" => "required_if:completiondate,selected|nullable|date",
            "requirement_others" => "",
            "application" => "required_if:request_type,rq01",
            "criticality" => "required",
            "others" => "",
            "requestonbehalf" => "",
            "description" => "",
            "sendtoHOU" => "",
            "sendtoHOD" => "",
            "file_path" => "nullable|mimes:pdf,jpeg,png,docx,xlsx|max:5242",
        ], [
            'requirement_others.required_if' => 'Requirement Type is required',
        ], [
            'requestonbehalf' => 'Request on behalf',
            'ownerapproval' => 'Required Owner Approval',
            'completiondate' => 'Required Completion Date'
        ]);

        $user_request = Ticket_IT::create($request->all());
        // dd($user_request);

        if($request->file())
        {
            $fileName = Carbon::now()->format('Y-m-d').'_'.time().'_'.$request->file('file_path')->getClientOriginalName();
            $filePath = $request->file('file_path')->storeAs('uploads', $fileName, 'public');
            FileStorage::create([
                'name' => $fileName,
                'file_path' => $filePath,
                'user_created' => $request->user_created,
                'user_updated' => $request->user_lastmaintain,
                'ticket_id' => $user_request->id,
            ]);
        }

        $insert_log = Ticket_IT_Log::create([
            'urf_no' => $user_request->id,
            'urf_status' => $request->urf_status,
            'user_created' => $request->user_created,
            'user_lastmaintain' => $request->user_lastmaintain,
        ]);

        if(!$user_request->sendtoHOU){
            $mail = Mail::to($user_request->sendtoHOD); /* HOU */
            if($user_request->requestor_fullname){
                $mail->cc($user_request->user_email, $user_request->requestor_email);
                /* Requestor & Requestor On Behalf */
            }else{
                $mail->cc($user_request->user_email);
            }
            // ->bcc($request->user_email /* ets-noreply */)
            $mail->send(new URFSent($user_request->id));
        }else{
            $mail = Mail::to($user_request->sendtoHOU); /* HOU */
            if($user_request->requestor_fullname){
                $mail->cc($user_request->user_email, $user_request->requestor_email);
                /* Requestor & Requestor On Behalf */
            }else{
                $mail->cc($user_request->user_email);
            }
            // ->bcc($request->user_email /* ets-noreply */)
            $mail->send(new URFSent($user_request->id));
        }

        return redirect()->route('urf_control.index')->with('msg', 'success')->with('status', $user_request->urf_status)->with('verifier', $user_request->sendtoHOU)->with('approver', $user_request->sendtoHOD);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getParameter($parameter_name){
        return Parameter::select('param_code', 'param_desc')
        ->where('category_code', $parameter_name)
        ->orderBy('param_desc')
        ->pluck('param_desc', 'param_code');
    }
}
