<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Mail\RegisterEmail;
use App\Http\Controllers\Controller;
use App\Parameter;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use Illuminate\Foundation\Auth\RegistersUsers;
use PhpParser\Builder\Param;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function registerEmail()
    {
        $passmin = Parameter::where('category_code', 'security')->where('param_code', 'password_length')->first()->param_name;
        $passUpp = Parameter::where('category_code', 'security')->where('param_code', 'Uppercase')->first()->param_desc;
        $passLow = Parameter::where('category_code', 'security')->where('param_code', 'Lowercase')->first()->param_desc;
        $passSpe = Parameter::where('category_code', 'security')->where('param_code', 'Specialcharacter')->first()->param_desc;
        $passNum = Parameter::where('category_code', 'security')->where('param_code', 'Number')->first()->param_desc;
        // $passExp = Parameter::where('category_code', 'security')->where('param_code', 'password_expiry')->first()->param_name;

        $validate = Validator::make(request()->all(), [
            'staffno' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'confirmed', 'min:'.$passmin, 'regex:/^'.$passUpp.$passLow.$passSpe.$passNum.'/']
        ], [
            'staffno' => 'Invalid Staff No.',
            'email' => 'Invalid E-Mail Address'
        ])->validate();

        $user_verify = User::select('id', 'username', 'fullname', 'email', 'status')
        ->where('id', $validate['staffno'])
        ->where('email', $validate['email'])->first();

        if(!$user_verify){
            return redirect()
            ->back()
            ->withInput()
            ->withErrors(['invalid_email' => 'Invalid Staff No. or Email address.']);
        }

        if($user_verify->status == "emp_stat01"){
            return redirect()
            ->back()
            ->withInput()
            ->withErrors(['activated' => 'Staff User is already activated. Please proceed to Login.']);
        }

        $user_verify->update([
            'password' => Hash::make($validate['password'])
        ]);

        // $user_verified->update([
        //     'status' => 1
        // ]);

        // $status = $user_verify->first()->status ?? 0;
        // dd($user_verify->first());

        Mail::to($user_verify->email)
        // ->bcc('itscol.noreply@gmail.com')
        ->send(new RegisterEmail($user_verify->id));

        return redirect()->route('registerStaff')
        ->with('msg', 'success')->with('email', $user_verify->email);
    }
}
