<?php

namespace App\Http\Controllers;

use App\Parameter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class MaintainParamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // $data = Student::latest()->get();
            $data = DB::table('parameters')->distinct()->orderBy('category_code')->get();
            
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    // $actionBtn = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm">Delete</a>';
                    $actionBtn = '<a href="'. route('maintain_param.edit', $row->id ) .'"><button class="btn btn-primary btn-sm"><i class="mdi mdi-magnify"></i></button></button></a></td>';                                     
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('maintenance.parameter.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('maintenance.parameter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $create = Parameter::create($request->all());
        return redirect()->route('maintain_param.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parameters = Parameter::where('id','=', $id)
        ->distinct()
        ->first();

        return view('maintenance.parameter.edit', compact('parameters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Parameter::where('id', '=', $id)
        ->update([
            'category_code' => $request->category_code,
            'param_code' => $request->param_code,
            'param_name' => $request->param_name,
            'param_desc' => $request->param_desc,
            'user_lastmaintain' => $request->user_lastmaintain
        ]);
        return redirect()->route('maintain_param.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->has('deleteparam'))
        {
            Parameter::where('id', $id)
            ->delete();
        }
        
        return redirect()->route('maintain_param.index');
    }
}
