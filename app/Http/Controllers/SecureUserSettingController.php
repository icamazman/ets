<?php

namespace App\Http\Controllers;

use App\User;
use App\UserLog;
use App\Parameter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\Facades\DataTables;

class SecureUserSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // $data = Student::latest()->get();
            $data = User::distinct()->with(['departments', 'units', 'branches', 'user_status', 'roles_param'])->orderBy('fullname')->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function($row){
                    return $row->user_status->param_desc;
                })
                ->addColumn('action', function($row){
                    // $actionBtn = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm">Delete</a>';
                    $actionBtn = '<a href="'. route('secure_usersetting.edit', $row->id ) .'"><button class="btn btn-primary btn-sm"><i class="mdi mdi-magnify"></i></button></button></a></td>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('security.secure_usersetting.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::with(['departments', 'units', 'branches', 'user_status', 'roles_param', 'roles'])
        ->distinct()
        ->where('id', $id)->first();
        // dd($users);

        $unit = $this->getParameter('unit');
        $branch = $this->getParameter('branch');
        $department = $this->getParameter('department');
        $roles = $this->getRole();


        return view('security.secure_usersetting.edit', compact('users', 'unit', 'branch', 'department', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->has('changerole'))
        {
            UserLog::create([
                'user_id' => Auth::id(),
                'attempt' => 'att08',
                'user_created' => Auth::user()->fullname,
                'user_lastmaintain' => Auth::user()->fullname,
            ]);

            $user = User::find($id);
            $user->syncRoles($request->role_id);
        }

        if($request->has('changepass')){
            $validatepass = $request->validate([
                'password' => 'required|min:8|confirmed',
            ]);

            UserLog::create([
                'user_id' => Auth::id(),
                'attempt' => 'att05',
                'user_created' => Auth::user()->fullname,
                'user_lastmaintain' => Auth::user()->fullname,
            ]);

            $changepassword = User::where('id', $id)->update(['password' => bcrypt($request->password)]);
        }

        if($request->has('changeava')){
            if($request->file('avatar_path')){
                $fileName = Carbon::now()->format('Y-m-d').'_'.$request->file('avatar_path')->getClientOriginalName();
                $filePath = $request->file('avatar_path')->storeAs('storage/avatars', $fileName, 'public');
                $img = Image::make($request->file('avatar_path')->getRealPath());
                $img->resizeCanvas(1500, 1500, 'center');
                // $img->greyscale();
                $img->save(public_path('images/faces/'.$fileName));
                // dd($img);
            }

            UserLog::create([
                'user_id' => Auth::id(),
                'attempt' => 'att01',
                'user_created' => Auth::user()->fullname,
                'user_lastmaintain' => Auth::user()->fullname,
            ]);

            User::where('id', $id)->update([
                'avatar_name' => $fileName,
                'avatar_path' => $filePath
            ]);
        }

        if($request->has('changedept')){
            UserLog::create([
                'user_id' => Auth::id(),
                'attempt' => 'att02',
                'user_created' => Auth::user()->fullname,
                'user_lastmaintain' => Auth::user()->fullname,
            ]);

            User::where('id', $id)->update([
                'department' => $request->department,
            ]);
        }

        if($request->has('changeunit')){
            UserLog::create([
                'user_id' => Auth::id(),
                'attempt' => 'att03',
                'user_created' => Auth::user()->fullname,
                'user_lastmaintain' => Auth::user()->fullname,
            ]);

            User::where('id', $id)->update([
                'unit' => $request->unit,
            ]);
        }

        if($request->has('changebranch')){
            UserLog::create([
                'user_id' => Auth::id(),
                'attempt' => 'att04',
                'user_created' => Auth::user()->fullname,
                'user_lastmaintain' => Auth::user()->fullname,
            ]);

            User::where('id', $id)->update([
                'branch' => $request->branch,
            ]);
        }

        return redirect()->route('secure_usersetting.index')->with('msg', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getParameter($parameter_name){
        return Parameter::select('param_code', 'param_desc')
        ->where('category_code', $parameter_name)
        ->orderBy('param_desc')
        ->pluck('param_desc', 'param_code');
    }

    public function getRole(){
        return Role::select('id', 'name')
        ->orderBy('id')
        ->pluck('name', 'id');
    }
}
