<?php

namespace App\Http\Controllers;

use App\User;
use App\Ticket_IT;
use Carbon\Carbon;
use App\Ticket_IT_Log;
use Illuminate\Http\Request;
use App\Exports\TicketExport;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;


class ReportMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Ticket_IT::with(['users', 'req_users', 'departments', 'req_departments', 'request_types', 'requirement_types', 'applications', 'criticalities','status'])
            ->distinct()
            ->first();

        if ($request->ajax()) {
            // $data = Student::latest()->get();
            $data = Ticket_IT::with(['users', 'req_users', 'departments', 'req_departments', 'request_types', 'requirement_types', 'applications', 'criticalities','status'])
            ->distinct()
            ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('urf_no', function($row){
                    // if( in_array($row->urf_status, array('stat01', 'stat02', 'stat03', 'stat05', 'stat08', 'stat13')) )
                    // {
                        return "URF".sprintf('%05d', $row->id);
                    // }else
                    //     return "ETS".sprintf('%05d', $row->urf_no);
                })
                ->addColumn('fullname', function($row){
                    return $row->users->fullname;
                })
                ->addColumn('mergeColumn', function($row){
                    if($row->request_type == "rq01" || $row->request_type == "Application"){
                        return $row->request_types->param_desc." - ".$row->applications->param_desc;
                    }
                    else{
                        return $row->request_types->param_desc;
                    }
                })
                ->addColumn('urf_status', function($row){
                    if($row->urf_status == "stat12"){
                        if($row->assignTo == "ass01"){
                            return $row->status->param_desc." - Assigned to ".$row->officers->fullname;
                        }else{
                            return $row->status->param_desc." - Assigned to Vendor";
                        }
                    }else{
                        return $row->status->param_desc;
                    }
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.route('report_master.show', $row->id).'"><button class="btn btn-primary btn-sm downloadPDF"><i class="mdi mdi-file-pdf"></i></button></a></td>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('report.report_master.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Download Master Report
        if($request->has('masterReport')){
            return Excel::download(new TicketExport, 'ticket.xlsx');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tracking = Ticket_IT::with([
            'users',
            'req_users',
            'departments',
            'req_departments',
            'request_types',
            'requirement_types',
            'applications',
            'criticalities',
            'status',
            'assignees',
            'officers',
            'complexities',
            'requirement_types_it',
            'owners',
        ])->where('id', $id)->first();
        // dd($tracking);
        $log = Ticket_IT_Log::with('status', 'tickets')
        ->where('urf_no', '=', $id)
        ->distinct()
        ->get();
            // dd($log);

        // foreach($log as $log){
        //     dd($log);
        // }
        $pdf = PDF::loadView('report.pdf', compact('tracking', 'log'))->setOptions(['defaultFont' => 'sans-serif']);
        // dd($pdf);

        // return $pdf->stream('tracking.pdf');
        // dd($pdf);
        $fileName =  time().'.'. 'pdf' ;
        $pdf->save($fileName);

        $pdf = public_path($fileName);
        return response()->download($pdf);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // $tracking = Ticket_IT::where('id', $id)->with([
        //     'users',
        //     'req_users',
        //     'departments',
        //     'req_departments',
        //     'request_types',
        //     'requirement_types',
        //     'applications',
        //     'criticalities',
        //     'status',
        //     'assignees',
        //     'officers',
        //     'complexities',
        //     'requirement_types_it',
        //     'owners',
        // ])->distinct()->first();

        // $users = User::query()
        // ->when($request->requestor_fullname, function($query, $requestor_id){
        //     return $query->where('id', $requestor_id);
        // });

        // if ($request->ajax() && $request->requestor_fullname) {
        //     $department = $users->first()->departments->param_desc;
        //     $users = $users->first();
        //     return ['department' => $department, 'users' => $users];
        // }

        // $users = $users->get();


        // return view('report.report_master.edit', compact('tracking', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
