<?php

namespace App\Http\Controllers;

use App\User;
use App\Parameter;
use App\Ticket_IT;
use Carbon\Carbon;
use App\FileStorage;
// use Hashids\Hashids;
use App\Mail\URFSent;
use App\Ticket_IT_Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Vinkla\Hashids\Facades\Hashids;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;


class StaffTrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // $data = Student::latest()->get();
            $data = Ticket_IT::where(function($q){ $q->where('urf_status', '!=', 'stat11')->where('user_dept', '=', Auth::user()->department); })
            ->with(['users', 'req_users', 'departments', 'req_departments', 'request_types', 'requirement_types', 'applications', 'criticalities','status', 'officers'])
            ->distinct()
            ->get();

            // dd($data);

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('urf_no', function($row){
                    // if( in_array($row->urf_status, array('stat01', 'stat02', 'stat03', 'stat05', 'stat08', 'stat13')) )
                    // {
                        return "URF".sprintf('%05d', $row->id);
                    // }else
                    //     return "ETS".sprintf('%05d', $row->urf_no);
                })
                ->addColumn('fullname', function($row){
                    return $row->users->fullname;
                })
                ->addColumn('mergeColumn', function($row){
                    if($row->request_type == "rq01"){
                        return $row->request_types->param_desc." - ".$row->applications->param_desc;
                    }
                    else{
                        return $row->request_types->param_desc;
                    }
                })
                ->addColumn('urf_status', function($row){
                    if($row->urf_status == "stat12"){
                        if($row->assignTo == "ass01"){
                            return $row->status->param_desc." - Assigned to ".$row->officers->fullname;
                        }else{
                            return $row->status->param_desc." - Assigned to Vendor";
                        }
                    }else{
                        return $row->status->param_desc;
                    }
                })
                ->addColumn('created_at', function($row){
                    return $row->created_at ? with(new Carbon($row->created_at))->format('d/m/Y h:m') : '';
                })
                ->addColumn('updated_at', function($row){
                    return $row->updated_at ? with(new Carbon($row->updated_at))->format('d/m/Y h:m') : '';
                })
                ->addColumn('action', function($row){
                    // $actionBtn = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm">Delete</a>';
                    $actionBtn = '<a href="'. route('staff_tracking.edit', $row->id) .'"><button class="btn btn-primary btn-sm"><i class="mdi mdi-magnify"></i></button></button></a></td>';
                    // $actionBtn = '';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('staff.staff_tracking.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $tracking = Ticket_IT::with(['users', 'req_users', 'departments', 'req_departments', 'request_types', 'requirement_types', 'applications', 'criticalities','status', 'officers', 'files'])
        ->distinct()
        ->where('id', $id)->first();

        // dd($tracking);

        if ($request->ajax()) {
            // dd($request->ajax());
            $data = Ticket_IT_Log::with('status', 'tickets')
            ->where('urf_no', $id)->get();
            // dd($id);
            // dd($data);

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('urf_no', function($row){
                    return "URF".sprintf('%05d', $row->urf_no);
                })
                ->addColumn('urf_status', function($row){
                    return $row->status->param_desc;
                })
                ->editColumn('updated_at', function ($row) {
                    return $row->updated_at ? with(new Carbon($row->updated_at))->format('d/m/Y h:m') : '';
                })
                ->make(true);
        }

        $applications = $this->getParameter('applications');
        $request_type = $this->getParameter('request_type');
        $requirement_type = $this->getParameter('requirement_type');
        $critically = $this->getParameter('critically')->sortKeys();
        $department = $this->getParameter('department');

        return view('staff.staff_tracking.edit', compact('tracking', 'applications', 'request_type', 'requirement_type', 'critically', 'department'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->has('resubmit')){ //stat13
            // $request->validate(["file_path" => "nullable|mimes:pdf,jpeg,png,docx,xlsx|max:5242",]);
            $amend = Ticket_IT::where('id', $id)
            ->update([
                'request_type' => $request->request_type,
                'requirement_type' => $request->requirement_type,
                'application' => $request->application,
                'others' => $request->others,
                'description' => $request->description,
                'criticality' => $request->criticality,
                'required_completion_date' => $request->required_completion_date,
                'urf_status' => $request->urf_status,
                'user_updated' => $request->user_lastmaintain,
            ]);

            if($request->file())
            {
                $fileName = Carbon::now()->format('Y-m-d').'_'.time().'_'.$request->file('file_path')->getClientOriginalName();
                $filePath = $request->file('file_path')->storeAs('uploads', $fileName, 'public');
                $file = FileStorage::create([
                    'name' => $fileName,
                    'file_path' => $filePath,
                    'user_created' => $request->user_created,
                    'user_updated' => $request->user_lastmaintain,
                    'ticket_id' => $id,
                ]);
            }

            // dd($request->file());
            $insert_log = Ticket_IT_Log::create($request->all());

            $ticket = Ticket_IT::where('id', $id)->first();

            $mail = Mail::to($ticket->sendtoHOU); /* HOU */
            if($ticket->requestor_fullname){
                $mail->cc($ticket->user_email, $ticket->requestor_email);
                /* Requestor & Requestor On Behalf */
            }else{
                $mail->cc($ticket->user_email);
            }
            // ->bcc($request->user_email /* ets-noreply */)
            $mail->send(new URFSent($ticket->id));

            $owner = null;
        }

        if($request->has('requestowner')){ //stat18
            $sendreqowner = Ticket_IT::where('id','=', $id)
            ->update([
                'required_owner_approval' => $request->required_owner_approval,
                'urf_status' => $request->urf_status,
                'user_updated' => $request->user_lastmaintain,
            ]);

            $insert_log = Ticket_IT_Log::create($request->all());

            $owner = User::where('department', $request->required_owner_approval)->where('unit', null)->select('email')->pluck('email')->first();

            $ticket = Ticket_IT::where('id', $id)->first();

            $mail = Mail::to($owner); /* HOD */
            if($ticket->requestor_fullname){
                $mail->cc($ticket->user_email, $ticket->requestor_email);
                /* Requestor & Requestor On Behalf */
            }else{
                $mail->cc($ticket->user_email);
            }
            // ->bcc($request->user_email /* ets-noreply */)
            $mail->send(new URFSent($ticket->id));
        }

        if($request->has('completerequest')){ //stat16
            $complete = Ticket_IT::where('id','=', $id)
            ->update([
                'urf_status' => $request->urf_status,
                'user_lastmaintain' => $request->user_lastmaintain,
            ]);

            $insert_log = Ticket_IT_Log::create($request->all());

            $ticket = Ticket_IT::with('officers')->where('id', $id)->first();

            $app = User::where('department', 'dept05')->where('username', 'hanita')->orWhere('username', 'rafidah')->orWhere('username', 'fairoz')->select('email')->pluck('email');
            $hdwr = User::where('department', 'dept05')->where('username', 'budi')->orWhere('username', 'nizam')->select('email')->pluck('email');
            $othr = User::where('department', 'dept05')->where('username', 'hanita')->orWhere('username', 'budi')->orWhere('username', 'siti')->orWhere('username', 'rafidah')->orWhere('username', 'fairoz')->orWhere('username', 'nizam')->orWhere('username', 'shamsul')->select('email')->pluck('email');

            $mail = Mail::to($ticket->sendto); //Officer In-Charge or Clerk
            if($ticket->request_type == "rq01")
            {
                $mail->cc($app);
            }
            if($ticket->request_type == "rq02")
            {
                $mail->cc($hdwr);
            }
            if($ticket->request_type == "rq03")
            {
                $mail->cc($othr);
            }
            // ->bcc($request->user_email /* ets-noreply */)
            $mail->send(new URFSent($ticket->id));

            $owner = null;

        }

        if($request->has('incompleterequest')){ //stat15
            $complete = Ticket_IT::where('id','=', $id)
            ->update([
                'urf_status' => $request->urf_status,
                'user_lastmaintain' => $request->user_lastmaintain,
            ]);

            $insert_log = Ticket_IT_Log::create($request->all());

            $ticket = Ticket_IT::with('officers')->where('id', $id)->first();

            $app = User::where('department', 'dept05')->where('username', 'hanita')->orWhere('username', 'rafidah')->orWhere('username', 'fairoz')->select('email')->pluck('email');
            $hdwr = User::where('department', 'dept05')->where('username', 'budi')->orWhere('username', 'nizam')->select('email')->pluck('email');
            $othr = User::where('department', 'dept05')->where('username', 'hanita')->orWhere('username', 'budi')->orWhere('username', 'siti')->orWhere('username', 'rafidah')->orWhere('username', 'fairoz')->orWhere('username', 'nizam')->orWhere('username', 'shamsul')->select('email')->pluck('email');

            $mail = Mail::to($ticket->officers->email); //Officer In-Charge
            if($ticket->request_type == "rq01")
            {
                $mail->cc($app);
            }
            if($ticket->request_type == "rq02")
            {
                $mail->cc($hdwr);
            }
            if($ticket->request_type == "rq03")
            {
                $mail->cc($othr);
            }
            // ->bcc($request->user_email /* ets-noreply */)
            $mail->send(new URFSent($ticket->id));

            $owner = null;

        }

        return redirect()->route('staff_tracking.index')->with('msg', 'success')->with('status', $request->urf_status)->with('owner', $owner);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getParameter($parameter_name){
        return Parameter::select('param_code', 'param_desc')
        ->where('category_code', $parameter_name)
        ->orderBy('param_desc')
        ->pluck('param_desc', 'param_code');
    }

    private function getModel($model, $routeKey)
    {
        $id = Hashids::connection($model)->decode($routeKey)[0] ?? null;
        $modelInstance = resolve($model);

        return  $modelInstance->findOrFail($id);
    }
}
