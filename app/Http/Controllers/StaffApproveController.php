<?php

namespace App\Http\Controllers;

use App\User;
use App\Parameter;
use App\Ticket_IT;
use Carbon\Carbon;
use App\Mail\URFSent;
use App\Ticket_IT_Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;

class StaffApproveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // $data = Student::latest()->get();
            $data = Ticket_IT::where('urf_status', 'stat02')->orWhere('urf_status', 'stat18')->orWhere('urf_status', 'stat22')->where('sendtoHOD', Auth::user()->email)
            // $data = Ticket_IT::whereIn('urf_status', 'stat02', 'stat18', 'stat22')->where('sendtoHOD', Auth::user()->email)
            ->with(['users', 'req_users', 'departments', 'req_departments', 'request_types', 'requirement_types', 'applications', 'criticalities','status'])
            ->distinct()
            ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('urf_no', function($row){
                    // if( in_array($row->urf_status, array('stat01', 'stat02', 'stat03', 'stat05', 'stat08', 'stat13')) ){
                        return "URF".sprintf('%05d', $row->id);
                    // }else{
                    //     return "ETS".sprintf('%05d', $row->urf_no);
                    // }
                })
                ->addColumn('fullname', function($row){
                    return $row->users->fullname;
                })
                ->addColumn('mergeColumn', function($row){
                    if($row->request_type == "rq01" || $row->request_type == "Application"){
                        return $row->request_types->param_desc." - ".$row->applications->param_desc;
                    }
                    else{
                        return $row->request_types->param_desc;
                    }
                })
                ->addColumn('urf_status', function($row){
                    if($row->urf_status == "stat12"){
                        if($row->assignTo == "ass01"){
                            return $row->status->param_desc." - Assigned to ".$row->officers->fullname;
                        }else{
                            return $row->status->param_desc." - Assigned to Vendor";
                        }
                    }else{
                        return $row->status->param_desc;
                    }
                })
                ->addColumn('created_at', function($row){
                    return $row->created_at ? with(new Carbon($row->created_at))->format('d/m/Y h:m') : '';
                })
                ->addColumn('updated_at', function($row){
                    return $row->updated_at ? with(new Carbon($row->updated_at))->format('d/m/Y h:m') : '';
                })
                ->addColumn('action', function($row){
                    // $actionBtn = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm">Delete</a>';
                    $actionBtn = '<a href="'. route('staff_approve.edit', $row->id ) .'"><button class="btn btn-primary btn-sm"><i class="mdi mdi-magnify"></i></button></button></a></td>';
                    // $actionBtn = '';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }


        return view('staff_approve.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $tracking = Ticket_IT::where('id','=', $id)
        ->with(['users', 'req_users', 'departments', 'req_departments', 'request_types', 'requirement_types', 'applications', 'criticalities','status', 'files'])
        ->distinct()
        ->first();

        if ($request->ajax()) {
            // $data = Student::latest()->get();
            $data = Ticket_IT_Log::with('status', 'tickets')
            ->where('urf_no', '=', $id)
            ->distinct()
            ->get();

            // dd($data);

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('urf_no', function($row){
                    return "URF".sprintf('%05d', $row->urf_no);
                })
                ->addColumn('urf_status', function($row){
                    return $row->status->param_desc;
                })
                ->editColumn('updated_at', function ($data) {
                    return $data->updated_at ? with(new Carbon($data->updated_at))->format('d/m/Y h:m') : '';;
                })
                ->make(true);
        }

        return view('staff_approve.edit', compact('tracking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $approve = Ticket_IT::where('id','=', $id)
        ->update([
            'urf_status' => $request->urf_status,
            'user_lastmaintain' => $request->user_lastmaintain
        ]);

        $app = User::where('department', 'dept05')->where('username', 'hanita')->select('email')->pluck('email')->first();
        $hdwr = User::where('department', 'dept05')->where('username', 'budi')->select('email')->pluck('email')->first();
        $othr = User::where('department', 'dept05')->where('username', 'hanita')->orWhere('username', 'budi')->select('email')->pluck('email');

        $insert_log = Ticket_IT_Log::create($request->all());

        $ticket = Ticket_IT::where('id', $id)->first();

        if($request->has('approveHOU'))
        {
            $urf_no = Ticket_IT::orderBy('urf_no', 'desc')->select('urf_no')->first()->urf_no;
            $update = Ticket_IT::where('id', $id)->update(['urf_no' => $urf_no + 1]);

            if($ticket->request_type == "rq01")
            {
                $mail = Mail::to($app);
            }
            if($ticket->request_type == "rq02")
            {
                $mail = Mail::to($hdwr);
            }
            if($ticket->request_type == "rq03")
            {
                $mail = Mail::to($othr);
            }

            if($ticket->requestor_fullname){
                $mail->cc($ticket->user_email, $ticket->requestor_email);
                 /* Requestor & Requestor On Behalf */
            }else{
                 $mail->cc($ticket->user_email);
            }
            $mail->send(new URFSent($id));

        }

        if($request->urf_status == "stat05")
        {
            if($ticket->requestor_fullname){
                $mail = Mail::to($ticket->user_email, $ticket->requestor_email);
                 /* Requestor & Requestor On Behalf */
             }else{
                $mail = Mail::to($ticket->user_email);
             }
            $mail->send(new URFSent($id));
        }

        if($request->has('approveOwner'))
        {
            if($ticket->request_type == "rq01")
            {
                $mail = Mail::to($app);
            }
            if($ticket->request_type == "rq02")
            {
                $mail = Mail::to($hdwr);
            }
            if($ticket->request_type == "rq03")
            {
                $mail = Mail::to($othr);
            }

            if($ticket->requestor_fullname){
                $mail->cc($ticket->user_email, $ticket->requestor_email);
                 /* Requestor & Requestor On Behalf */
            }else{
                 $mail->cc($ticket->user_email);
            }
            $mail->send(new URFSent($id));
        }

        return redirect()->route('staff_approve.index')->with('msg', 'success')->with('status', $request->urf_status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
