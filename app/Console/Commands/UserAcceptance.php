<?php

namespace App\Console\Commands;

use App\User;
use App\Ticket_IT;
use App\Mail\URFSent;
use App\Ticket_IT_Log;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserAcceptance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:UserAcceptance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update a pending user acceptance after 7 days as Compelete';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $update = Ticket_IT::join('ticket_it_log', 'ticket_it.id', '=', 'ticket_it_log.urf_no')
        ->where('ticket_it.urf_status', 'stat14')->where('ticket_it_log.urf_status', 'stat14');

        $id = $update->select('ticket_it.id')->pluck('ticket_it.id')->first();

        $insert_log = Ticket_IT_Log::create([
            'urf_no' => $id,
            'urf_status' => 'stat16',
            'user_created' => 'System',
            'user_lastmaintain' => 'System'
        ]);

        $update->whereRaw('ticket_it_log.created_at < DATE_ADD(ticket_it_log.created_at, INTERVAL 7 DAY)')
        ->update(['ticket_it.urf_status' => 'stat16']);

        $stat16 = Ticket_IT::join('ticket_it_log', 'ticket_it.id', '=', 'ticket_it_log.urf_no')
        ->where('ticket_it.urf_status', 'stat16')->where('ticket_it_log.urf_status', 'stat16');

        $app = User::where('department', 'dept05')->where('username', 'hanita')->orWhere('username', 'rafidah')->orWhere('username', 'fairoz')->select('email')->pluck('email');
        $hdwr = User::where('department', 'dept05')->where('username', 'budi')->orWhere('username', 'nizam')->select('email')->pluck('email');
        $othr = User::where('department', 'dept05')->where('username', 'hanita')->orWhere('username', 'budi')->orWhere('username', 'siti')->orWhere('username', 'rafidah')->orWhere('username', 'fairoz')->orWhere('username', 'nizam')->orWhere('username', 'shamsul')->select('email')->pluck('email');

        $id = $stat16->select('ticket_it.id')->pluck('ticket_it.id')->first();

        $mail = Mail::to($stat16->select('ticket_it.sendto')->pluck('ticket_it.sendto')->first()); //Officer In-Charge or Clerk
        if($stat16->select('ticket_it.request_type')->pluck('ticket_it.request_type')->first() == "rq01")
        {
            $mail->cc($app);
        }
        if($stat16->select('ticket_it.request_type')->pluck('ticket_it.request_type')->first() == "rq02")
        {
            $mail->cc($hdwr);
        }
        if($stat16->select('ticket_it.request_type')->pluck('ticket_it.request_type')->first() == "rq03")
        {
            $mail->cc($othr);
        }
        // ->bcc($request->user_email /* ets-noreply */)
        $mail->send(new URFSent($id));
    }
}
