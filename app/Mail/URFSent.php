<?php

namespace App\Mail;

use App\Ticket_IT;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class URFSent extends Mailable
{
    use Queueable, SerializesModels;
    public $ticket_it;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {

        $id = Ticket_IT::with(['users', 'req_users', 'departments', 'req_departments', 'request_types', 'requirement_types', 'applications', 'criticalities', 'status', 'officers'])
        ->find($id);

        $this->ticket_it = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // if( in_array($this->ticket_it->urf_status, array('stat01', 'stat02', 'stat03', 'stat05', 'stat08', 'stat13', 'stat22')) ){
        //     if ($this->ticket_it->request_type == "rq01") {
        //         return $this->from('punb.ets@gmail.com', 'e-Ticketing System')
        //                 ->subject('eTS: '.$this->ticket_it->request_types->param_desc.' '.$this->ticket_it->applications->param_desc.' (Ref: URF'.sprintf('%05d', $this->ticket_it->id).') - '.$this->ticket_it->status->param_desc)
        //                 ->view('mail.mail');
        //     }

        //     if($this->ticket_it->request_type == "rq02" || $this->ticket_it->request_type == "rq03") {
        //         return $this->from('punb.ets@gmail.com', 'e-Ticketing System')
        //                 ->subject('eTS: '.$this->ticket_it->request_types->param_desc.' (Ref: URF'.sprintf('%05d', $this->ticket_it->id).') - '.$this->ticket_it->status->param_desc)
        //                 ->view('mail.mail');
        //     }
        // }
        // else{
        //     if ($this->ticket_it->request_type == "rq01") {
        //         return $this->from('punb.ets@gmail.com', 'e-Ticketing System')
        //                 ->subject('eTS: '.$this->ticket_it->request_types->param_desc.' '.$this->ticket_it->applications->param_desc.' (Ref: ETS'.sprintf('%05d', $this->ticket_it->urf_no).') - '.$this->ticket_it->status->param_desc)
        //                 ->view('mail.mail');
        //     }

        //     if($this->ticket_it->request_type == "rq02" || $this->ticket_it->request_type == "rq03") {
        //         return $this->from('punb.ets@gmail.com', 'e-Ticketing System')
        //                 ->subject('eTS: '.$this->ticket_it->request_types->param_desc.' (Ref: ETS'.sprintf('%05d', $this->ticket_it->urf_no).') - '.$this->ticket_it->status->param_desc)
        //                 ->view('mail.mail');
        //     }
        // }

        if ($this->ticket_it->request_type == "rq01") {
            return $this->from('punb.ets@gmail.com', 'e-Ticketing System')
                    ->subject('eTS: '.$this->ticket_it->request_types->param_desc.' '.$this->ticket_it->applications->param_desc.' (Ref: URF'.sprintf('%05d', $this->ticket_it->id).') - '.$this->ticket_it->status->param_desc)
                    ->view('mail.mail');
        }

        if($this->ticket_it->request_type == "rq02" || $this->ticket_it->request_type == "rq03") {
            return $this->from('punb.ets@gmail.com', 'e-Ticketing System')
                    ->subject('eTS: '.$this->ticket_it->request_types->param_desc.' (Ref: URF'.sprintf('%05d', $this->ticket_it->id).') - '.$this->ticket_it->status->param_desc)
                    ->view('mail.mail');
        }
    }
}
