<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    protected $table = 'parameters';
    protected $fillable = [
        'category_code',
        'param_code',
        'param_name',
        'param_desc',
        'user_created',
        'user_lastmaintain'
    ];

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function tickets()
    {
        return $this->belongsTo(Ticket_IT::class);
    }

    public function ticket_logs()
    {
        return $this->belongsTo(Ticket_IT_Log::class);
    }

    public function attempt_param()
    {
        return $this->belongsTo(UserLog::class);
    }
}
