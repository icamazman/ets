<?php

namespace App;

use PhpParser\Builder\Param;
use App\Http\Traits\Hashidable;
use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Database\Eloquent\Model;

class Ticket_IT extends Model
{
    // use Hashidable;

    // protected $appends = ['hashed_id'];

    protected $table='ticket_it';
    protected $fillable = [
            'urf_no',
            'user_fullname',
            'user_dept',
            'user_email',
            'user_ext',
            'requestor_fullname',
            'requestor_dept',
            'requestor_email',
            'requestor_ext',
            'request_type',
            'requirement_type',
            'application',
            'others',
            'description',
            'criticality',
            'required_completion_date',
            'required_owner_approval',
            'sendtoHOU',
            'sendtoHOD',
            'urf_status',
            'user_created',
            'user_lastmaintain',
            'assignTo',
            'dtassign',
            'officer',
            'requirement_type_it',
            'complexity',
            'dt_ictd_testing',
            'dt_user_testing',
            'others_it'
    ];

    public function users()
    {
        return $this->hasOne(User::class, 'id', 'user_fullname');
    }

    public function req_users()
    {
        return $this->hasOne(User::class, 'id', 'requestor_fullname');
    }

    public function departments()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'user_dept');
    }

    public function req_departments()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'requestor_dept');
    }

    public function request_types()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'request_type');
    }

    public function requirement_types()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'requirement_type');
    }

    public function applications()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'application');
    }

    public function criticalities()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'criticality');
    }

    public function status()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'urf_status');
    }

    public function assignees()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'assignTo');
    }

    public function officers()
    {
        return $this->hasOne(User::class, 'id', 'officer');
    }

    public function ticket_it_logs()
    {
        return $this->belongsTo(Ticket_IT_Log::class);
    }

    public function complexities()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'complexity');
    }

    public function requirement_types_it()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'requirement_type_it');
    }

    public function owners()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'required_owner_approval');
    }

    public function files()
    {
        return $this->hasOne(FileStorage::class, 'ticket_id', 'id');
    }

    public function HOUemails()
    {
        return $this->hasOne(User::class, 'sendtoHOU', 'email');
    }

    public function HODemails()
    {
        return $this->hasOne(User::class, 'sendtoHOD', 'email');
    }

    // public function getHashedIdAttribute()
    // {
    //     return Hashids::connection(get_called_class())->encode($this->getKey());
    // }
}
