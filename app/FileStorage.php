<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileStorage extends Model
{
    protected $table = 'file_storage';
    protected $fillable = [
        'name',
        'file_path',
        'user_created',
        'user_updated',
        'ticket_id'
    ];

    public function files()
    {
        return $this->belongsTo(FileStorage::class);
    }
}
