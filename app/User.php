<?php

namespace App;

use App\Http\Traits\Hashidable;
use Vinkla\Hashids\Facades\Hashids;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    // use Hashidable;

    // protected $appends = ['hashed_id'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'username',
            'fullname',
            'email',
            'department',
            'unit',
            'branch',
            'ext_phone_no',
            'role',
            'status',
            'access_expired',
            'password',
            'user_created',
            'user_lastmaintain',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function departments()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'department');
    }

    public function units()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'unit');
    }

    public function branches()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'branch');
    }

    public function user_status()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'status');
    }

    public function roles_param()
    {
        return $this->hasOne(Parameter::class, 'param_code', 'role');
    }

    public function HOUemails()
    {
        return $this->belongsTo(Ticket_IT::class);
    }

    public function HODemails()
    {
        return $this->belongsTo(Ticket_IT::class);
    }

    public function officers()
    {
        return $this->belongsTo(User::class);
    }

    public function users_log()
    {
        return $this->belongsTo(UserLog::class);
    }

    // public function getHashedIdAttribute()
    // {
    //     return Hashids::connection(get_called_class())->encode($this->getKey());
    // }

}
