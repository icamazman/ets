<?php

use App\User;
use App\Parameter;
use App\Ticket_IT;
use App\Ticket_IT_Log;

/**
 * Copyright (c) Vincent Klaiber.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://github.com/vinkla/laravel-hashids
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'main',

    /*
    |--------------------------------------------------------------------------
    | Hashids Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        // User::class => [
        //     'salt' => User::class.'7623e9b0009feff8e024a689d6ef59ce',
        //     'length' => '10',
        //     // 'alphabet' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        // ],

        // Ticket_IT::class => [
        //     'salt' => Ticket_IT::class.'7623e9b0009feff8e024a689d6ef59ce',
        //     'length' => '12',
        //     // 'alphabet' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        // ],

        // Parameter::class => [
        //     'salt' => Parameter::class.'7623e9b0009feff8e024a689d6ef59ce',
        //     'length' => '8',
        // ],

        // Ticket_IT_Log::class => [
        //     'salt' => Ticket_IT_Log::class.'7623e9b0009feff8e024a689d6ef59ce',
        //     'length' => '12',
        //     // 'alphabet' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        // ],

    ],

];
