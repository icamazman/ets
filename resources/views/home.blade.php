@extends('layouts.app_layout')

@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-8">
                <div class="home-tab">
                    {{-- <div class="d-sm-flex align-items-center justify-content-between border-bottom">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                            <a class="nav-link active ps-0" id="home-tab" data-bs-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#audiences" role="tab" aria-selected="false">Profile</a>
                            </li>
                        </ul>
                    </div> --}}
                    <div class="tab-content tab-content-basic">
                        <h1>User Guide</h1>
                        <div class="accordion accordion-flush" id="accordionFlushExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingOne">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                        User Request Form (F-001)
                                    </button>
                                </h2>
                                <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <b>Requestor Info</b><br><br>
                                        - Requestor Info is filled automatically as you authorizatedly logged in. <br>
                                        - Other than that, you may fill in for someone else by clicking on "Request on Behalf."<br>
                                        <br>
                                        <b>Request Details</b> <br> <br>
                                        <i>1. Request Type</i> <br>
                                        &ensp; - Application: Request upon systems, e.g: FMS, CODA, IMAS and etc. <br>
                                        &ensp; - Hardware: Request upon any computer hardware, e.g: Laptop, Printer and etc. <br>
                                        &ensp; - Others: Request upon other than two types above, e.g: Software and etc. <br>
                                        <br>
                                        <i>2. Requirement Type</i> <br>
                                        &ensp; - Change: <br>
                                        &ensp; - Data Extraction: <br>
                                        &ensp; - New: <br>
                                        &ensp; - Others: <br>
                                        <br>
                                        <i>3. Criticality</i> <br>
                                        &ensp; - Low: <br>
                                        &ensp; - Medium: <br>
                                        &ensp; - High: <br>
                                        <br>
                                        <i>4. Attachment</i> <br>
                                        &ensp; - File type supported: docx, xlsx, pdf <br>
                                        &ensp; - Maximum size file upload: 5MB <br>
                                        <br>
                                        <i>5. Description</i> <br>
                                        &ensp; - Describe upon your request with details.
                                        <br><br>
                                        <i>6. Required Completion Date</i> <br>
                                        &ensp; - Optional. <br>
                                        &ensp; - Place a date for URF to be settled. <br>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                    Change Avatar
                                    </button>
                                </h2>
                                <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the second item's accordion body. Let's imagine this being filled with some actual content.</div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                    Change Password
                                    </button>
                                </h2>
                                <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the third item's accordion body. Nothing more exciting happening here in terms of content, but just filling up the space to make it look, at least at first glance, a bit more representative of how this would look in a real-world application.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- content-wrapper ends -->

</div>
@endsection
