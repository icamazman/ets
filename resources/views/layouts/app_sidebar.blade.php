<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-category">Navigation</li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('home') }}">
        <i class="mdi menu-icon mdi mdi-home"></i>
        <span class="menu-title">Home</span>
      </a>
    </li>
    {{-- <li class="nav-item">
      <a class="nav-link" href="index.html">
        <i class="mdi mdi mdi-view-grid menu-icon"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li> --}}
    <li class="nav-item nav-category">Staff</li>
    <li class="nav-item">
      <a class="nav-link" data-bs-toggle="collapse" href="#ui-staff" aria-expanded="false" aria-controls="ui-staff">
        <i class="menu-icon mdi mdi-file-document-box"></i>
        <span class="menu-title">Form</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-staff">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{ route('urf_control.index') }}">User Request Form (F-001)</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('staff_tracking.index') }}">Tracking URF</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('staff_draft.index') }}">Draft URF</a></li>
        </ul>
      </div>
    </li>

    @hasanyrole('HOU|ICT HOU|Super Admin')
    <li class="nav-item">
      <a class="nav-link" data-bs-toggle="collapse" href="#ui-verify" aria-expanded="false" aria-controls="ui-verify">
        <i class="menu-icon mdi mdi mdi-check-circle"></i>
        <span class="menu-title">Verification</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-verify">
        <ul class="nav flex-column sub-menu">
            @can('urf_verify')
            <li class="nav-item"> <a class="nav-link" href="{{ route('staff_verify.index') }}">URF Verification</a></li>
            @endcan
            @can('urf_ict_verify')
            <li class="nav-item"> <a class="nav-link" href="{{ route('ict_verify.index') }}">URF IT Verification</a></li>
            @endcan
        </ul>
      </div>
    </li>
    @endhasanyrole

    @hasanyrole('HOD|ICT HOD|Super Admin')
    <li class="nav-item">
      <a class="nav-link" data-bs-toggle="collapse" href="#ui-approve" aria-expanded="false" aria-controls="ui-approve">
        <i class="menu-icon mdi mdi-checkbox-multiple-marked-circle"></i>
        <span class="menu-title">Approval</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-approve">
        <ul class="nav flex-column sub-menu">
          @can('urf_approve')
              <li class="nav-item"> <a class="nav-link" href="{{ route('staff_approve.index') }}">URF Approval</a></li>
          @endcan
          @can('urf_ict_approve')
              <li class="nav-item"><a class="nav-link" href="{{ route('ict_approve.index') }}"> URF IT Approval</a></li>
          @endcan
        </ul>
      </div>
    </li>
    @endhasanyrole

    @unlessrole('Staff|HOU|HOD|Team Leader|Admin')
    <li class="nav-item nav-category">ICT Side</li>

    @hasanyrole('ICT HOU|ICT Staff|Super Admin')
    <li class="nav-item">
      <a class="nav-link" data-bs-toggle="collapse" href="#ui-ict" aria-expanded="false" aria-controls="ui-ict">
        <i class="menu-icon mdi mdi-grease-pencil"></i>
        <span class="menu-title">Assignments</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-ict">
        <ul class="nav flex-column sub-menu">
            {{-- @can('task_assign') --}}
            <li class="nav-item"> <a class="nav-link" href="{{ route('ict_assign.index') }}">Task IT Assignation</a></li>
            {{-- @endcan --}}
            {{-- @can('person_in-charge') --}}
            <li class="nav-item"> <a class="nav-link" href="{{ route('ict_pic.index') }}">IT Person In-Charge</a></li>
            {{-- @endcan --}}
        </ul>
      </div>
    </li>
    @endhasanyrole

    @hasanyrole('Admin|Super Admin')
    <li class="nav-item">
      <a class="nav-link" data-bs-toggle="collapse" href="#ui-maintain" aria-expanded="false" aria-controls="ui-maintain">
        <i class="menu-icon mdi mdi mdi-settings"></i>
        <span class="menu-title">Maintenance Side</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-maintain">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{ route('maintain_param.index') }}">Parameter</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ route('maintain_urf.index') }}">URF Maintenance</a></li>
        </ul>
      </div>
    </li>
    @endhasanyrole

    @endunlessrole


    <li class="nav-item nav-category">Settings</li>

    <li class="nav-item">
      <a class="nav-link" data-bs-toggle="collapse" href="#ui-security" aria-expanded="false" aria-controls="ui-security">
        <i class="menu-icon mdi mdi-account-settings"></i>
        <span class="menu-title">Settings</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-security">
        <ul class="nav flex-column sub-menu">
            @can('profile')
                <li class="nav-item"> <a class="nav-link" href="{{ route('secure_profile.index') }}">Profile</a></li>
            @endcan
            @can('user_setting')
                <li class="nav-item"> <a class="nav-link" href="{{ route('secure_usersetting.index') }}">User Setting</a></li>
            @endcan
            @can('site_security')
                <li class="nav-item"> <a class="nav-link" href="{{ route('secure_site.index') }}">Site Security</a></li>
            @endcan
            @can('email_setting')
                <li class="nav-item"> <a class="nav-link" href="{{ route('secure_email.index') }}">Email Setting</a></li>
            @endcan
        </ul>
      </div>
    </li>

    @hasanyrole('Admin|Super Admin')
    <li class="nav-item nav-category">Report</li>
    <li class="nav-item">
      <a class="nav-link" data-bs-toggle="collapse" href="#ui-report" aria-expanded="false" aria-controls="ui-report">
        <i class="menu-icon mdi mdi-library-books"></i>
        <span class="menu-title">Report</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="ui-report">
        <ul class="nav flex-column sub-menu">
            @can('master_report')
                <li class="nav-item"> <a class="nav-link" href="{{ route('report_master.index') }}">Master Report</a></li>
            @endcan
        </ul>
      </div>
    </li>
    @endhasanyrole

    <!-- <li class="nav-item">
      <a class="nav-link" href="index.html">
        <i class="mdi mdi-grid-large menu-icon"></i>
        <span class="menu-title">Logout</span>
      </a>
    </li> -->

  </ul>
</nav>
