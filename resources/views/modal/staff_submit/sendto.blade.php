<div class="modal fade sendto" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Submit URF</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label>
                    Send to HOU:
                </label>
                <select class="form-control" name="sendtoHOU" id="verifiername">
                    <option selected value=""></option>
                    @foreach($verifier as $id)
                    <option value="{{$id->email}}">{{$id->fullname}}</option>
                    @endforeach
                </select>
                <br>
                <label>
                    Send to HOD:
                </label>
                <select class="form-control" name="sendtoHOD" id="approvername">
                    <option selected disabled></option>
                    @foreach($approver as $id)
                    <option value="{{$id->email}}">{{$id->fullname}}</option>
                    @endforeach
                </select>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success" data-bs-toggle="modal" data-bs-target=".submit">Submit</a>
                <button type="button" class="btn btn-danger close" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
            </div>
        </div>
    </div>
</div>
