<div class="modal fade accept" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">User Acceptance</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label>Are you sure that you accept your request resolved?</label>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" type="submit" name="completerequest">Yes</button>
                <a class="btn btn-danger" data-bs-toggle="modal" data-bs-target=".incomplete">No</a>
            </div>
        </div>
    </div>
</div>
