<div class="modal fade incomplete" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Incomplete Request</h5>
                <button class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label>Remark</label>
                <textarea class="form-control-lg col-md-12" id="exampleTextarea1" rows="5" name="remark"></textarea>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" type="submit" name="incompleterequest">Yes</button>
                <a class="btn btn-danger close" data-bs-dismiss="modal" aria-label="Close">Cancel</a>
            </div>
        </div>
    </div>
</div>
