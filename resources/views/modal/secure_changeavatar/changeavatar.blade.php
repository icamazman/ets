<div class="modal fade changeavatar" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Password</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label class="form-check-label">
                    New Avatar
                </label>
                <input class="form-control form-control-sm" id="formFileSm" type="file" name="avatar_path"/>
                @error('avatar_path')
                    <div class="text-danger small">{{ $message }}</div>
                @enderror
            </div>
            <div class="modal-footer">
                <input type="hidden" class="form-control" name="user_lastmaintain" value="{{ Auth::user()->fullname }}"/>
                <button class="btn btn-success" type="submit" name="changeava">Save Changes</button>
                <button type="button" class="btn btn-danger close" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
            </div>
        </div>
    </div>
</div>
