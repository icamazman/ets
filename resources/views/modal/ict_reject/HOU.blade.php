<div class="modal fade reject" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Reject</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('ict_verify.update', $tracking->id) }}" >
                @method('put')
                @csrf
                <div class="modal-body">
                    <!-- <label>Please choose one reason</label>
                    <select class="form-control my-3" name="required_owner_approval">
                        <option selected>Select one request type</option>
                        <option>Application</option>
                        <option>Hardware</option>
                        <option>Others</option>
                    </select> -->
                    <input type="hidden" class="form-control" name="user_created" value="{{ Auth::user()->fullname }}"/>
                    <input type="hidden" class="form-control" name="user_lastmaintain" value="{{ Auth::user()->fullname }}"/>
                    <input type="hidden" class="form-control" name="urf_status" value="stat07"/>
                    <input type="hidden" class="form-control" name="urf_no" value="{{ $tracking->id }}"/>
                    <label>Remark</label>
                    <textarea class="form-control-lg col-md-12" id="exampleTextarea1" rows="5" name="remark"></textarea>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" name="icthoureject">Submit</button>
                    <button type="button" class="btn btn-danger close" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
