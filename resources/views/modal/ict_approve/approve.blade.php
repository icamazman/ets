<div class="modal fade approve" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Approve</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('ict_approve.update', $tracking->id) }}" >
                @method('put')
                @csrf
                <div class="modal-body">
                    <label>Are you sure you want to proceed?</label>
                </div>
                <div class="modal-footer">
                    <input type="hidden" class="form-control" name="user_created" value="{{ Auth::user()->fullname }}"/>
                    <input type="hidden" class="form-control" name="user_lastmaintain" value="{{ Auth::user()->fullname }}"/>
                    <input type="hidden" class="form-control" name="urf_status" value="stat09"/>
                    <input type="hidden" class="form-control" name="urf_no" value="{{ $tracking->id }}"/>
                    <button class="btn btn-success" type="submit">Submit</button>
                    <button type="button" class="btn btn-danger close" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
