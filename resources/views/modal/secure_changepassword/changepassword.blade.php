<div class="modal fade changepassword" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Password</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label class="form-check-label">
                    New Password
                </label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required/>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <br>
                <label class="form-check-label">
                    Confirm Password
                </label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" required/>
            </div>
            <div class="modal-footer">
                <input type="hidden" class="form-control" name="user_lastmaintain" value="{{ Auth::user()->fullname }}"/>
                <button class="btn btn-success" type="submit" name="changepass">Save Changes</button>
                <button type="button" class="btn btn-danger close" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
            </div>
        </div>
    </div>
</div>
