<div class="modal fade verify" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">ICT Verification</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Requirement Type</label>
                    <div class="align-self-center">
                        <select class="form-control" name="requirement_type_it" id="requirement_type" onchange="selection();">
                            <option selected value="" disabled>Select one requirement type</option>
                            @foreach($requirement_type_it as $id => $value)
                            <option value="{{$id}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group" id="requirement_others" style="display: none;">
                    <label>Requirement Type: Others</label>
                    <div class="align-self-center">
                        <input type="text" class="form-control" name="others_it"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label>Complexity</label>
                    @foreach($complexity as $id => $value)
                    <div class="col-sm-4">
                        <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="complexity" id="membershipRadios1" value="{{$id}}">
                        {{$value}}
                        </label>
                        </div>
                    </div>
                    @endforeach
                </div>
                <label class="form-check-label">
                    ICTD Testing Date:
                </label>
                <input type="date" class="form-control" name="dt_ictd_testing"/>
                <br>
                <label class="form-check-label">
                    User Testing Date:
                </label>
                <input type="date" class="form-control" name="dt_user_testing"/>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success" data-bs-toggle="modal" data-bs-target=".hod">Submit</a>
                <button class="btn btn-danger close" type="button" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
            </div>
        </div>
    </div>
</div>
