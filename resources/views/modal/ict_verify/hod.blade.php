<div class="modal fade hod" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">ICT Verification</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label>Does it need IT HOD's approval?</label>
            </div>
            <div class="modal-footer">
                <input type="hidden" class="form-control" name="user_created" value="{{ Auth::user()->fullname }}"/>
                <input type="hidden" class="form-control" name="user_lastmaintain" value="{{ Auth::user()->fullname }}"/>
                <input type="hidden" class="form-control" name="urf_status"/>
                <input type="hidden" class="form-control" name="urf_no" value="{{ $tracking->id }}"/>
                <button class="btn btn-success need" type="submit" name="ictverifywith">Yes</button>
                <button class="btn btn-danger donotneed" type="submit" name="ictverifywithout">No</button>
            </div>
        </div>
    </div>
</div>
