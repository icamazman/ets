<div class="modal fade request" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Required Owner Approval</h5>
                <button class="close" type="button" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form action="{{ route('ict_verify.update', $tracking->id) }}" method="POST">
                @method('put')
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="user_created" class="form-control" value="{{ Auth::user()->fullname }}">
                    <input type="hidden" name="user_lastmaintain" class="form-control" value="{{ Auth::user()->fullname }}">
                    <input type="hidden" name="urf_status" class="form-control" value="stat08">
                    <input type="hidden" name="urf_no" class="form-control" value="{{ $tracking->id }}">
                    <label for="remark">Remark</label>
                    <textarea name="remark" class="form-control-lg col-md-12" rows="5"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" name="requiredownerapproval">Submit</button>
                    <button type="button" class="btn btn-danger close" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
