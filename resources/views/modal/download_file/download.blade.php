<div class="modal fade download" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">File Attached</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if($tracking->files)
                <li>{{$tracking->files->name}}</li>
                @else
                <p>No file attached</p>
                @endif
            </div>
            <div class="modal-footer">
                @if($tracking->files)
                    <a class="btn btn-warning download m-2" href="{{asset('storage/uploads/'.$tracking->files->name)}}" target="_blank">View</a>
                    <a class="btn btn-success download m-2" href="{{asset('storage/uploads/'.$tracking->files->name)}}" download>Download</a>
                @endif
                <button type="button" class="btn btn-danger close" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
            </div>
        </div>
    </div>
</div>
