<div class="modal fade delete" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('maintain_param.destroy', $parameters->id) }}" >
                @method('delete')
                @csrf
                <div class="modal-body">
                    <label>Are you sure you want to proceed?</label>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" name="deleteparam">Yes</button>
                    <button type="button" class="btn btn-danger close" data-bs-dismiss="modal" aria-label="Close">No</button>
                </div>
            </form>
        </div>
    </div>
</div>