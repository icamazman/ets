<div class="modal fade resolve" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Resolve</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('ict_pic.update', $tracking->id) }}" >
                @method('put')
                @csrf
                <div class="modal-body">
                    <label>Impact Statement:</label>
                    <br>
                    <small>List down and/or briefly describe actions to be taken</small>
                    <textarea class="form-control-lg col-md-12" id="exampleTextarea1" rows="5" name="remark"></textarea>
                </div>
                <div class="modal-footer">
                    <input type="hidden" class="form-control" name="user_created" value="{{ Auth::user()->fullname }}"/>
                    <input type="hidden" class="form-control" name="user_lastmaintain" value="{{ Auth::user()->fullname }}"/>
                    <input type="hidden" class="form-control" name="urf_status" value="stat14"/>
                    <input type="hidden" class="form-control" name="urf_no" value="{{ $tracking->id }}"/>
                    <button class="btn btn-success" type="submit" name="resolveofficer">Submit</button>
                    <button class="btn btn-danger close" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
