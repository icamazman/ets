<div class="modal fade assign" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Assign task</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('ict_assign.update', $tracking->id) }}">
                @method('put')
                @csrf
                <div class="modal-body">
                    <label>Assign to:</label>
                    <select class="form-control" id="assignTo" name="assignTo" onchange="selection();">
                        <option selected> </option>
                        @foreach($assignee as $id => $value)
                        <option value="{{$id}}">{{$value}}</option>
                        @endforeach
                    </select>
                    <label class="mt-3" id="labelictd" style="display: none;">ICTD:</label>
                    <select class="form-control" name="officer" id="ictd" style="display: none;">
                        <option selected> </option>
                        @foreach($officer as $off => $value)
                        <option value="{{$off}}">{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <input type="hidden" class="form-control" name="user_created" value="{{ Auth::user()->fullname }}"/>
                    <input type="hidden" class="form-control" name="user_lastmaintain" value="{{ Auth::user()->fullname }}"/>
                    <input type="hidden" class="form-control" name="urf_status" value="stat12"/>
                    <input type="hidden" class="form-control" name="urf_no" value="{{ $tracking->id }}"/>
                    <input type="hidden" name="dtassign" value="{{Carbon\Carbon::now()->format('Y-m-d')."T".Carbon\Carbon::now()->format('H:i')}}">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger close" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
