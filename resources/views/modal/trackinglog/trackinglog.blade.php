<div class="modal fade modal-trackinglog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tracking Log</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped yajra-datatable">
                <thead>
                    <tr>
                    <th>

                    </th>
                    <th>
                        URF No.
                    </th>
                    <th>
                        URF Status
                    </th>
                    <th>
                        Remark
                    </th>
                    <th>
                        Last Maintain
                    </th>
                    <th>
                        Created By
                    </th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
