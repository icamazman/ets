@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Staff Account Verification</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('verifyingStaff', $user->id) }}">
                        @csrf
                        <div class="form-group row mb-0">
                            <div class="col" style="text-align: center">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Verify Account') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
