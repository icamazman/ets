@extends('layouts.app_layout')

@section('content')
        <div class="content-wrapper">
          <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Approve URF</h4>
                  <p class="card-description">
                    <!-- Add class <code>.table-bordered</code> -->
                  </p>
                  <div class="table-responsive pt-3">
                  <table class="table table-bordered table-striped yajra-datatable">
                      <thead>
                        <tr>
                          <th>

                          </th>
                          <th>
                            URF No.
                          </th>
                          <th>
                            Full Name
                          </th>
                          <th>
                            Request Type
                          </th>
                          <th>
                            Status
                          </th>
                          <th>
                            Created at
                          </th>
                          <th>
                            Updated at
                          </th>
                          <th>

                          </th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
           </div>
        </div>
@endsection

@include('modal/staff_submit/modalSubmit')

@push('js')
<script type="text/javascript">
    $(function () {
        var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('staff_approve.index') }}",
        columns: [
        {data: 'DT_RowIndex', name: 'DT_RowIndex'},
        {data: 'urf_no', name: 'urf_no'},
        {data: 'fullname', name: 'fullname'},
        {data: 'mergeColumn', name: 'mergeColumn'},
        {data: 'urf_status', name: 'urf_status'},
        {data: 'created_at', name: 'created_at'},
        {data: 'updated_at', name: 'updated_at'},
        {data: 'action', name: 'action', orderable: true, searchable: true},
        ]});
    });

    @if(session('msg') && session('status') == "stat04")
        $('.urf-submit .modal-body').html('<span class="text-success">URF successfully has been approved. This request will proceed to ICT HOU for ICT Verification.</span>');
        myModal.show();
    @endif

    @if(session('msg') && session('status') == "stat05")
        $('.urf-submit .modal-body').html('<span class="text-danger">URF has been rejected and will notify to Requestor that URF is cancelled.</span>');
        myModal.show();
    @endif
</script>
@endpush
