<!DOCTYPE html>
<html>
<head>
    <title>Export row data to PDF</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<style type="text/css">
    body{
        font-family: 'Roboto', sans-serif;
        font-size: 12px
    }
    th, td {
        padding: 5px;
        /* border: 1px solid black; */
    }
    #header-table td{
        border: 0px;
    }
</style>
<body>
    <table id="header-table" style="width: 100%;">
        <tr>
            <td><h4>PERBADANAN USAHAWAN NASIONAL BERHAD</h4></td>
            <td style="text-align: right">
                <p style="font-size: 8px">PROPRIETARY</p><br>
                <p>
                    Reference No: @if( in_array($tracking->urf_status, array('stat01', 'stat02', 'stat03', 'stat05', 'stat08', 'stat13')) )
                        URF{{sprintf('%05d', $tracking->id)}}
                    @else
                        ETS{{sprintf('%05d', $tracking->urf_no)}}
                    @endif
                </p>
            </td>
        </tr>
    </table>
    <hr>
    <sub>Information & Communication Technology Deparment</sub>
    <h3 style="text-align: center">USER REQUEST FORM (F-001)</h3>
    <sup>Note: Failure to complete this form completely and legibly may result in delay of processing or the return of your request.</sup><br>
    <sup>(This section is to be completed by the requestor.)</sup>
    <table class="table table-bordered" style="width: 100%">
        <tr>
            <td> <strong>Requestor Name:</strong>  <br> {{ $tracking->users->fullname }}</td>
            <td> <strong>Requestor Department:</strong>  <br> {{ $tracking->departments->param_desc }}</td>
        </tr>
        <tr>
            <td> <strong>Requestor Email:</strong>  <br> {{ $tracking->user_email }}</td>
            <td> <strong>Requestor Ext Phone No:</strong>  <br> {{ $tracking->user_ext }}</td>
        </tr>
        @if ($tracking->req_users)
        <tr>
            <td>REQUEST ON BEHALF</td>
        </tr>
        <tr>
            <td> <strong>Requestor Name:</strong>  <br> {{ $tracking->req_users->fullname }}</td>
            <td> <strong>Requestor Department:</strong>  <br> {{ $tracking->req_departments->param_desc }}</td>
        </tr>
        <tr>
            <td> <strong>Requestor Email:</strong>  <br> {{ $tracking->requestor_email }}</td>
            <td> <strong>Requestor Ext Phone No:</strong>  <br> {{ $tracking->requestor_ext }}</td>
        </tr>
        @endif
        <tr>
            <td @if ($tracking->request_type != "rq01") colspan="2" @endif><strong>Request Type:</strong> @if ($tracking->request_type == "rq01") <br> @endif {{ $tracking->request_types->param_desc }}</td>
            @if ($tracking->request_type == "rq01")<td> <strong>Application:</strong>  <br> {{ $tracking->applications->param_desc }}</td> @endif
        </tr>
        <tr>
            <td> <strong>Requirement Type:</strong> <br> {{ $tracking->requirement_types->param_desc }}</td>
            @if ($tracking->requirement_type == "rt07") <td><strong>Others: </strong> <br> {{ $tracking->others }}</td> @endif
            <td> <strong>Cricitality:</strong> <br> {{ $tracking->criticalities->param_desc }}</td>
        </tr>
        <tr><td colspan="2"> <strong>Description:</strong> <br> {{ $tracking->description }} </td></tr>
        <tr>
            <td> <strong>Required Completion Date:</strong> <br> {{ $tracking->required_completion_date }}</td>
            <td> <strong>Required Owner Apporval:</strong> <br> {{ $tracking->required_owner_approval }}</td>
        </tr>
        @if ($tracking->required_owner_approval != 0)
        <tr>
            <td><strong>Owner Approved by:</strong> <br> {{ $tracking->owners->fullname }} </td>
            @foreach ( $log as $log )
            @if ($log->urf_status == "stat19" && $tracking->id == $log->urf_no)
            <td><strong>Owner Approved at:</strong> <br> {{ $log->created_at }} </td>
            @endif
            @endforeach
        </tr>
        @endif
        @foreach ( $log as $log )
            @if ($log->urf_status == "stat02" && $tracking->id == $log->urf_no)
            <tr>
                <td><strong>HOU Verified by:</strong> <br> {{ $log->user_created }} </td>
                <td><strong>HOU Verified at:</strong> <br> {{ $log->created_at }}  </td>
            </tr>
            @endif
            @if ($log->urf_status == "stat03" && $tracking->id == $log->urf_no)
            <tr>
                <td><strong>HOU Rejected by:</strong> <br> {{ $log->user_created }}</td>
                <td><strong>HOU Rejected at:</strong> <br> {{ $log->created_at }}</td>
            </tr>
            <tr>
                <td><strong>Remark:</strong> <br> {{ $log->remark }}</td>
            </tr>
            @endif
            @if ($log->urf_status == "stat04" && $tracking->id == $log->urf_no)
            <tr>
                <td><strong>HOD Approved by:</strong>  <br>{{ $log->user_created }} </td>
                <td><strong>HOD Approved at:</strong> <br>{{ $log->created_at }} </td>
            </tr>
            @endif
            @if ($log->urf_status == "stat05" && $tracking->id == $log->urf_no)
            <tr>
                <td><strong>HOU Rejected by:</strong> <br> {{ $log->user_created }}</td>
                <td><strong>HOU Rejected at:</strong> <br> {{ $log->created_at }}</td>
            </tr>
            <tr>
                <td><strong>Remark:</strong> <br> {{ $log->remark }}</td>
            </tr>
            @endif
            {{-- <tr><td><sup>(This section is to be completed by the relevant ICT Department.)</sup></td></tr> --}}
            @if ( in_array($log->urf_status, array('stat06', 'stat17')) && $tracking->id == $log->urf_no)
            <tr>
                <td><strong>ICT HOU Verified by:</strong> <br> {{ $log->user_created }}</td>
                <td><strong>ICT HOU Verified at:</strong> <br> {{ $log->created_at }}</td>
            </tr>
            @endif
            @if ($log->urf_status == "stat07" && $tracking->id == $log->urf_no)
            <tr>
                <td><strong>ICT HOU Rejected by:</strong> <br> {{ $log->user_created }}</td>
                <td><strong>ICT HOU Rejected at:</strong> <br> {{ $log->created_at }}</td>
            </tr>
            <tr>
                <td><strong>Remark:</strong> <br> {{ $log->remark }}</td>
            </tr>
            @endif
            @if ($log->urf_status == "stat09" && $tracking->id == $log->urf_no )
            <tr>
                <td><strong>ICT HOD Approved by:</strong> <br> {{ $log->user_created }}</td>
                <td><strong>ICT HOD Approved by:</strong> <br> {{ $log->created_at }}</td>
            </tr>
            @endif
            @if ($log->urf_status == "stat10" && $tracking->id == $log->urf_no)
            <tr>
                <td><strong>ICT HOD Rejected by:</strong> <br> {{ $log->user_created }}</td>
                <td><strong>ICT HOD Rejected at:</strong> <br> {{ $log->created_at }}</td>
            </tr>
            <tr>
                <td><strong>Remark:</strong> <br> {{ $log->remark }}</td>
            </tr>
            @endif
            @if ($log->urf_status == "stat12" && $tracking->id == $log->urf_no && $tracking->assignTo == "ass01")
            <tr>
                <td><strong>Assigned to:</strong> <br>{{ $tracking->assignees->param_desc}} </td>
                <td><strong>Officer In-Charge:</strong> <br> {{ $tracking->officers->fullname }}</td>
            </tr>
            <tr>
                <td><strong>Requirement Type:</strong> <br> {{ $tracking->requirement_types_it->param_desc }}</td>
                @if ($tracking->requirement_types_it == "rt03")
                <td><strong>Requirement Type (Others):</strong> <br> {{ $tracking->others_it }}</td>
                @endif
                <td><strong>Complexity:</strong> <br> {{ $tracking->complexities->param_desc }} </td>
            </tr>
            <tr>
                <td><strong>ICTD Testing Date: </strong> <br> {{ $tracking->dt_ictd_testing }} </td>
                <td><strong>User Testing Date: </strong> <br> {{ $tracking->dt_user_testing }} </td>
            </tr>
            @endif
            @if ($log->urf_status == "stat12" && $tracking->id == $log->urf_no && $tracking->assignTo == "ass02")
            <tr>
                <td><strong>Assigned to:</strong> <br>{{ $tracking->assignees->param_desc }}</td>
            </tr>
            @endif
            @if ($log->urf_status == "stat14" && $tracking->id == $log->urf_no)
            <tr>
                <td><strong>Impact Statement:</strong> <br> {{ $log->remark }}</td>
            </tr>
            @endif
            @if ($log->urf_status == "stat16" && $tracking->id == $log->urf_no)
            <tr>
                <td><strong>User Acceptance</strong><br>I hereby confirm that all required actioons pertaining to this request have been satisfactorily completed and tested.</td>
            </tr>
            <tr>
                <td><strong>Accepted by:</strong> <br> </td>
                <td><strong>Accepted at:</strong> <br> </td>
            </tr>
            @endif
        @endforeach
    </table>
</body>
</html>
