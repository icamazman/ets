@extends('layouts.app_layout')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div class="p-2">
                            <h3 class="card-title">Master Report</h3>
                        </div>
                        <div class="p-2">
                            <form action="{{ route('report_master.store') }}" method="POST">@csrf
                                <button type="submit" class="btn btn-primary btn-sm" name="masterReport">Excel</button>
                            </form>
                        </div>
                    </div>
                    <p class="card-description">
                    <!-- Add class <code>.table-bordered</code> -->
                    </p>
                    <div class="table-responsive pt-3">
                        <table class="table table-bordered table-striped yajra-datatable">
                            <thead>
                                <tr>
                                    <th>

                                    </th>
                                    <th>
                                    URF No.
                                    </th>
                                    <th>
                                    Full Name
                                    </th>
                                    <th>
                                    Request Type
                                    </th>
                                    <th>
                                    Status
                                    </th>
                                    <th>

                                    </th>
                                </tr>
                            </thead>

                            <tbody>

                            </tbody>
                            </form>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
    $(".downloadPDF").click(function(){
    var data = '';
    $.ajax({
        type: 'GET',
        url: "{{ route('report_master.show', $data->id) }}",
        data: data,
        xhrFields: {
            responseType: 'blob'
        },
        success: function(response){
            var blob = new Blob([response]);
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = "Sample.pdf";
            link.click();
        },
        error: function(blob){
            console.log(blob);
        }
    });
    });
  $(function () {
    var table = $('.yajra-datatable').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('report_master.index') }}",
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'urf_no', name: 'urf_no'},
      {data: 'fullname', name: 'fullname'},
      {data: 'mergeColumn', name: 'mergeColumn'},
      {data: 'urf_status', name: 'urf_status'},
      {data: 'action', name: 'action', orderable: true, searchable: true},
    ]});
  });

</script>
@endpush
