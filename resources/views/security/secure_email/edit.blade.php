@extends('layouts.app_layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">User Profile</h4>
            <form class="form-sample">
              <p class="card-description">
                User Login
              </p>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Username</label>
                    <div class="align-self-center" >
                      <input type="text" class="form-control"/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Full Name</label>
                    <div class="align-self-center">
                      <input type="text" class="form-control"/>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>E-Mail Address</label>
                    <div class="align-self-center">
                      <input type="email" class="form-control"/>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Password</label>
                    <div class="align-self-center">
                      <input type="password" class="form-control"/>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <p class="card-description">
                User Details
              </p>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Department/Sect</label>
                    <div class="align-self-center">
                      <input type="text" class="form-control"/>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Unit</label>
                    <div class="align-self-center">
                      <input type="text" class="form-control"/>
                    </div>
                  </div>
                </div>                    
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Branch</label>
                    <div class="align-self-center">
                      <input type="text" class="form-control"/>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Role</label>
                    <div class="align-self-center">
                      <input type="text" class="form-control"/>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Designation</label>
                    <div class="align-self-center">
                      <input type="text" class="form-control"/>
                    </div>
                  </div>
                </div>                    
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Status</label>
                    <div class="align-self-center">
                      <input type="text" class="form-control"/>
                    </div>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-primary me-2">Submit</button>
              <button class="btn btn-light">Save to Drafts</button>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection