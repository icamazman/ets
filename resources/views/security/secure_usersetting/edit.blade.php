@extends('layouts.app_layout')

@section('content')

<div class="content-wrapper">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div class="p-2">
                            <h4 class="card-title">{{$users->fullname}}'s Profile</h4>
                            <p class="card-description">{{$users->fullname}}'s Details</p>
                        </div>
                        <div class="p-2">
                            <form method="POST" action="{{ route('secure_usersetting.update', $users->id) }}" enctype="multipart/form-data">
                                <img class="img-xs rounded-circle"
                                @if(!$users->avatar_name) src="{{ asset('images/faces/default.png') }}"
                                @else src="{{ asset('images/faces/'.$users->avatar_name) }}"
                                @endif alt="Profile image" >
                                    @method('put')
                                    @csrf
                                <a class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target=".changeavatar">Change Avatar</a>
                                @include('modal/secure_changeavatar/changeavatar')
                            </form>
                        </div>
                    </div>
                    <div class="form-sample">
                        <div class="row text-center">
                            {{-- <div class="col-md-6">
                                <div class="form-group">
                                    <label>Username</label>
                                    <div class="align-self-center" >
                                    <input type="text" class="form-control" value="{{$users->username}}" disabled/>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <div class="align-self-center">
                                    <input type="text" class="form-control text-center" value="{{$users->fullname}}" disabled/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>E-Mail Address</label>
                                    <div class="align-self-center">
                                    <input type="email" class="form-control text-center" value="{{$users->email}}" disabled/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <form method="POST" action="{{ route('secure_usersetting.update', $users->id) }}" >
                                    @method('put')
                                    @csrf
                                    <div class="form-group">
                                        <label>Password</label>
                                        <div class="align-self-center">
                                        <a class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target=".changepassword">Change Password</a>
                                        </div>
                                        @include('modal/secure_changepassword/changepassword')
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </form>
                            </div>
                            <form method="POST" action="{{ route('secure_usersetting.update', $users->id) }}" >
                                @method('put')
                                @csrf
                                <div class="col-md">
                                    <label>User's Role:</label>
                                    @foreach($roles as $id => $value)
                                    <label class="form-check-label mx-2">
                                        <input class="checkbox" type="checkbox" name="role_id[]" value="{{$id}}" @foreach ($users->roles()->select('id')->pluck('id') as $role_id) {{ $role_id == $id ? 'checked' : ''}} @endforeach>
                                        {{$value}}
                                    </label>
                                    @endforeach
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end mt-2">
                                    <button type="submit" class="btn btn-primary btn-sm" name="changerole">Save Changes</button>
                                </div>
                            </form>
                        </div>
                        <hr>
                        <div class="row">
                            <form method="POST" action="{{ route('secure_usersetting.update', $users->id) }}">
                                @method('put')
                                @csrf
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Department/Sect</label>
                                        <select class="form-control" name="department" id="department">
                                            <option value="{{$users->department}}" selected>{{$users->departments->param_desc}} - Selected</option>
                                            @foreach($department as $id => $value)
                                            <option value="{{$id}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button type="submit" class="btn btn-primary btn-sm" name="changedept">Save Changes</button>
                                </div>
                            </form>
                            <form method="POST" action="{{ route('secure_usersetting.update', $users->id) }}">
                                @method('put')
                                @csrf
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Unit</label>
                                        <select class="form-control" name="unit" id="unit">
                                            <option value="{{$users->unit}}" selected>{{$users->units->param_desc}} - Selected</option>
                                            @foreach($unit as $id => $value)
                                            <option value="{{$id}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button type="submit" class="btn btn-primary btn-sm" name="changeunit">Save Changes</button>
                                </div>
                            </form>
                            <form method="POST" action="{{ route('secure_usersetting.update', $users->id) }}">
                                @method('put')
                                @csrf
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Branch</label>
                                        <select class="form-control" name="branch" id="branch">
                                            <option value="{{$users->branch}}" selected>{{$users->branches->param_desc}} - Selected</option>
                                            @foreach($branch as $id => $value)
                                            <option value="{{$id}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                    <button type="submit" class="btn btn-primary btn-sm" name="changebranch">Save Changes</button>
                                </div>
                            </form>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Status </label>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="text" class="p-1 form-control-plaintext" value="{{$users->user_status->param_desc}}" disabled/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@include('modal/secure_changepassword/notsame')

@endsection

@push('js')

<script>
    var myModal = new bootstrap.Modal(document.querySelector('.notsame'), {});

    $(document).ready(function(){
        @if(session('msg'))
            $('.notsame .modal-body').html('<span class="text-success">Password changed successfully.</span>');
            myModal.show();
        @endif

        @if($errors->any())
            $('.notsame .modal-body').html('<span class="text-danger">Failed to change password. Check error message.</span>');
            myModal.show();
        @endif
    });
</script>

@endpush
