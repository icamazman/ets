@extends('layouts.app_layout')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">User Setting</h4>
                    <p class="card-description">
                    <!-- Add class <code>.table-bordered</code> -->
                    </p>
                    <div class="table-responsive pt-3">
                        <table class="table table-bordered table-striped yajra-datatable">
                            <thead>
                            <tr>
                                <th>

                                </th>
                                <th>
                                Username
                                </th>
                                <th>
                                Full Name
                                </th>
                                <th>
                                E-Mail
                                </th>
                                <th>
                                Ext. Phone No.
                                </th>
                                <th>
                                Status
                                </th>
                                <th>

                                </th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@include('modal/secure_changepassword/notsame')

@push('js')
<script type="text/javascript">
  $(function () {
    var table = $('.yajra-datatable').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('secure_usersetting.index') }}",
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'username', name: 'username'},
      {data: 'fullname', name: 'fullname'},
      {data: 'email', name: 'email'},
      {data: 'ext_phone_no', name: 'ext_phone_no'},
      {data: 'status', name: 'status'},
      {data: 'action', name: 'action', orderable: true, searchable: true},
    ]});
  });

  var myModal = new bootstrap.Modal(document.querySelector('.notsame'), {});
  $(document).ready(function(){
        @if(session('msg'))
            $('.notsame .modal-body').html('<span class="text-success">Changes has made successfully.</span>');
            myModal.show();
        @endif

        @if($errors->any())
            $('.notsame .modal-body').html('<span class="text-danger">Failed to make changes. Check error message.</span>');
            myModal.show();
        @endif
    });
</script>
@endpush
