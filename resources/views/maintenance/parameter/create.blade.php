@extends('layouts.app_layout')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Create New Parameter</h4>
                    <form class="form-sample" method="POST" action="{{ route('maintain_param.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Category Code</label>
                                    <div class="align-self-center" >
                                    <input type="text" class="form-control" name="category_code"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Paramater Code</label>
                                    <div class="align-self-center" >
                                    <input type="text" class="form-control" name="param_code"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Paramater Name</label>
                                    <div class="align-self-center" >
                                    <input type="text" class="form-control" name="param_name"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Paramater Description</label>
                                    <div class="align-self-center" >
                                    <input type="text" class="form-control" name="param_desc"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="user_created" class="form-control" value="system">
                        <input type="hidden" name="user_lastmaintain" class="form-control" value="system">
                        <div class="d-grid gap-2 d-md-flex justify-content-md-end m-3">
                            <button type="submit" class="btn btn-primary me-2">Add</button>
                            <a href="{{ route('maintain_param.index') }}" class="btn btn-primary me-2">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection