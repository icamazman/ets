@extends('layouts.app_layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="d-flex justify-content-between">
              <div class="p-2">
                <h4 class="card-title">Parameters</h4>
              </div>
              <div class="p-2">
                <a class="btn btn-primary" href="{{ route('maintain_param.create') }}">&plus; Create New Parameter</a>
              </div>
            </div>
          </div>
          <p class="card-description">
            <!-- Add class <code>.table-bordered</code> -->
          </p>
          <div class="table-responsive pt-3">
            <table class="table table-bordered table-striped yajra-datatable">
              <thead>
                <tr>
                  <th>

                  </th>
                  <th>
                    Category
                  </th>
                  <th>
                    Parameter Code
                  </th>
                  <th>
                    Description
                  </th>
                  <th>

                  </th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
  $(function () {
    var table = $('.yajra-datatable').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('maintain_param.index') }}",
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'category_code', name: 'category_code'},
      {data: 'param_code', name: 'param_code'},
      {data: 'param_desc', name: 'param_desc'},
      {data: 'action', name: 'action', orderable: true, searchable: true},
    ]});
  });
</script>
@endpush
