@extends('layouts.app_layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Parameter Details - {{ $parameters->param_desc }}</h4>
          <form class="form-sample" method="POST" action="{{ route('maintain_param.update', $parameters->id) }}">
            @method('put')
            @csrf
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Category Code</label>
                  <div class="align-self-center" >
                    <input type="text" class="form-control" name="category_code" value="{{ $parameters->category_code }}"/>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Paramater Code</label>
                  <div class="align-self-center" >
                    <input type="text" class="form-control" name="param_code" value="{{ $parameters->param_code }}"/>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Paramater Name</label>
                  <div class="align-self-center" >
                    <input type="text" class="form-control" name="param_name" value="{{ $parameters->param_name }}"/>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Paramater Description</label>
                  <div class="align-self-center" >
                    <input type="text" class="form-control" name="param_desc" value="{{ $parameters->param_desc }}"/>
                  </div>
                </div>
              </div>
            </div>
            <input type="hidden" name="user_lastmaintain" class="form-control" value="{{ Auth::user()->fullname }}">
            @include('modal/maintain/param/save')
          </form>
          <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <button class="btn btn-success" data-bs-toggle="modal" data-bs-target=".save">Save Changes</button>
            <button class="btn btn-danger" data-bs-toggle="modal" data-bs-target=".delete">Delete</button>
            @include('modal/maintain/param/delete')
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection
