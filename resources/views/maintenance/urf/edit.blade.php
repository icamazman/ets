@extends('layouts.app_layout')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                        <h4 class="card-title">User Request Form (F-001)</h4>
                        <small class="text-muted">
                            Note : Failure to complete this form completely and legibly may result in delay of processing or the return of your request.
                            <br> (This section is to be completed by the requester)
                        </small>
                        </div>
                        <div class="col-md-2">
                        <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target=".modal-trackinglog">Tracking Log</button>
                        </div>
                    </div>
                    <form class="form-sample" name="urf_store" action="" method="POST">
                        @method('put')
                        @csrf
                        <hr>
                        <p class="card-description">Requester Info</p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>User Full Name</label>
                                <div class="align-self-center" >
                                    <input type="text" class="form-control" name="user_fullname" value="{{ $tracking->users->fullname }}" disabled/>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Department/Sect</label>
                                <div class="align-self-center">
                                    <input type="text" class="form-control" name="user_dept" value="{{ $tracking->departments->param_desc }}" disabled/>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>E-Mail Address</label>
                                <div class="align-self-center">
                                    <input type="email" class="form-control" name="user_email" value="{{ $tracking->user_email }}" disabled/>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>Ext. Phone No.</label>
                                <div class="align-self-center">
                                    <input type="text" class="form-control" name="user_ext" value="{{ $tracking->user_ext }}" disabled/>
                                </div>
                                </div>
                            </div>
                        </div>

                        <p class="card-description"><input type="checkbox" class="form-check-input mx-2" id="requestonbehalf" {{ $tracking->requestor_fullname == 0 ? '' : 'checked' }}>Request on behalf</p>
                        <div class="row">
                            <div class="col-md-6" id="requestonbehalfform1">
                                <div class="form-group">
                                    <label>Requester Name</label>
                                    <div class="align-self-center" >
                                    @if($tracking->requestor_fullname == 1)
                                    <select name="requestor_fullname" class="form-control requestor_fullname">
                                        <option value="{{ $tracking->req_users->id }}" selected><strong>{{ $tracking->req_users->fullname }}</strong></option>
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}" @if($user->id == old('requestor_fullname')) selected @endif>{{$user->fullname}}</option>
                                        @endforeach
                                    </select>
                                    @else
                                    <select name="requestor_fullname" class="form-control requestor_fullname">
                                        <option value="" selected disabled></option>
                                        @foreach($users as $user)
                                        <option value="{{$user->id}}" @if($user->id == old('requestor_fullname')) selected @endif>{{$user->fullname}}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                    @error('requestor_fullname')
                                        <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" id="requestonbehalfform2">
                                <div class="form-group">
                                    <label>Department/Sect</label>
                                    <div class="align-self-center">
                                        @if($tracking->requestor_fullname == 1)
                                        <input type="text" class="form-control" name="display_dept" value="{{ $tracking->req_departments->param_desc }}" disabled>
                                        <input type="hidden" class="form-control" name="requestor_dept"/>
                                        @else
                                        <input type="text" class="form-control" name="display_dept" disabled>
                                        <input type="hidden" class="form-control" name="requestor_dept"/>
                                        @endif
                                    </div>
                                    @error('requestor_dept')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" id="requestonbehalfform3">
                                <div class="form-group">
                                    <label>E-Mail Address</label>
                                    <div class="align-self-center">
                                        @if($tracking->requestor_fullname == 1)
                                        <input type="email" class="form-control" name="requestor_email" value="{{ $tracking->requestor_email }}" disabled>
                                        <input type="hidden" class="form-control" name="requestor_email"/>
                                        @else
                                        <input type="email" class="form-control" name="requestor_email" disabled>
                                        <input type="hidden" class="form-control" name="requestor_email"/>
                                        @endif
                                    </div>
                                    @error('requestor_email')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6" id="requestonbehalfform4">
                                <div class="form-group">
                                    <label>Ext. Phone No.</label>
                                    <div class="align-self-center">
                                        @if($tracking->requestor_fullname == 1)
                                        <input type="text" class="form-control" name="requestor_ext" value="{{ $tracking->requestor_ext }}" disabled/>
                                        <input type="hidden" class="form-control" name="requestor_ext"/>
                                        @else
                                        <input type="text" class="form-control" name="requestor_ext" disabled/>
                                        <input type="hidden" class="form-control" name="requestor_ext"/>
                                        @endif
                                    </div>
                                    @error('requestor_ext')
                                    <div class="text-danger small">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <p class="card-description">Request Details</p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Request Type</label>
                                    <div class="align-self-center">
                                        <select class="form-control" name="request_type" id="request_type" onchange="selection();">
                                            <option value="{{ $tracking->request_type }}" {{ $tracking->request_type == 0 ? '' : 'selected'  }}>{{ $tracking->request_types->param_desc }}</option>
                                            @foreach($request_type as $id => $value)
                                            <option value="{{$id}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Requirement Type</label>
                                    <div class="align-self-center">
                                        <select class="form-control" name="requirement_type" id="requirement_type" onchange="selection();">
                                            <option value="{{ $tracking->requirement_type }}" {{ $tracking->requirement_type == 0 ? '' : 'selected'  }}>{{ $tracking->requirement_types->param_desc }}</option>
                                            @foreach($requirement_type as $id => $value)
                                            <option value="{{$id}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="application_type" @if($tracking->application == 0) style="display: none;" @else @endif>
                                    <label>Application</label>
                                    <div class="align-self-center" >
                                        <select class="form-control" name="application">
                                            <option value="{{ $tracking->application }}" {{ $tracking->application == 0 ? '' : 'selected'  }}>@if($tracking->application == 0) '' @else {{$tracking->applications->param_desc}} @endif </option>
                                            @foreach($applications as $id => $application)
                                                <option value="{{$id}}">{{$application}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="requirement_others" @if($tracking->others == 0) style="display: none;" @else @endif>
                                    <label>Requirement Type: Others</label>
                                    <div class="align-self-center">
                                    <input type="text" class="form-control" name="others" value="{{ $tracking->others }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label>Criticality</label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="criticality" id="membershipRadios1" value="c01" {{$tracking->criticality == "c01" ? 'checked' : '' }} >
                                            Low
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="criticality" id="membershipRadios2" value="c02" {{$tracking->criticality == "c02" ? 'checked' : ''}}>
                                                Medium
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="criticality" id="membershipRadios1" value="c03" {{$tracking->criticality == "c03" ? 'checked' : ''}}>
                                            High
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Attachment</label>
                                    <button class="btn btn-primary btn-sm p-1" data-bs-toggle="modal" data-bs-target=".download">View</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleTextarea1">Description</label>
                                    <textarea class="form-control-lg col-md-12" id="exampleTextarea1" rows="10" name="description" value="{{ $tracking->description }}"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" id="completiondate" {{ $tracking->required_completion_date == 0 ? '' : 'checked' }}>
                                    Required Completion Date
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" id="ownerapproval" {{ $tracking->required_owner_approval == 0 ? '' : 'checked' }}>
                                    Required Owner Approval
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                            <div class="align-self-center" id="completiondateform">
                                <input type="text" class="form-control" name="required_completion_date" value="{{$tracking->required_completion_date}}"/>
                            </div>
                            </div>
                            <div class="col-md-6 mb-3">
                            <div class="align-self-center" id="ownerapprovalform">
                                <select class="form-control" name="required_owner_approval">
                                    <option value="{{ $tracking->required_owner_approval }}" {{ $tracking->required_owner_approval == 0 ? '' : 'selected'  }}>
                                    @if($tracking->required_owner_approval == 0) @else {{$tracking->owners->param_desc}} @endif
                                </option>
                                @foreach($department as $id => $department)
                                    <option value="{{$id}}">{{$department}}</option>
                                @endforeach
                                </select>
                            </div>
                            </div>
                        </div>
                        <hr>
                        <h4>ICT Side</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Requirement Type</label>
                                    <div class="align-self-center">
                                        <select class="form-control" name="requirement_type_it">
                                            <option value="{{ $tracking->requirement_type_it }}" {{ $tracking->requirement_type_it == 0 ? '' : 'selected'  }}>
                                                @if($tracking->requirement_type_it == 0) @else {{$tracking->requirement_types_it->param_desc}} @endif
                                                @foreach($requirement_type as $id => $value)
                                                    <option value="{{$id}}">{{$value}}</option>
                                                @endforeach
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                @if(boolval($tracking->others_it) == 1)
                                <div class="form-group">
                                    <label>Requirement Type: Others</label>
                                    <div class="align-self-center">
                                        <input type="text" class="form-control" name="others" value="{{ $tracking->others_it }}"/>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label>Complexity</label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="complexity" id="membershipRadios1" value="co01" {{$tracking->complexity == "co01" ? 'checked' : ''}}>
                                            Low
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="complexity" id="membershipRadios2" value="co02" {{$tracking->complexity == "co02" ? 'checked' : ''}}>
                                                Medium
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="complexity" id="membershipRadios1" value="co03" {{$tracking->complexity == "co03" ? 'checked' : ''}}>
                                            High
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label> ICTD Testing Date:</label>
                                <input type="text" class="form-control" name="dt_ictd_testing" value="{{ $tracking->dt_ictd_testing }}"/>
                            </div>
                            <div class="col-md-6">
                                <label>User Testing Date:</label>
                                <input type="text" class="form-control" name="dt_user_testing" value="{{ $tracking->dt_user_testing }}"/>
                            </div>
                        </div>
                        </div>
                        <input type="hidden" class="form-control" name="user_lastmaintain" value="{{ Auth::user()->fullname }}"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('modal/trackinglog/trackinglog')
@include('modal/download_file/download')


@endsection

@push('js')
<script type="text/javascript">
  $(function () {
    var table = $('.yajra-datatable').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('maintain_urf.edit', $tracking->id) }}",
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'urf_no', name: 'urf_no'},
      {data: 'urf_status', name: 'urf_status'},
      {data: 'remark', name: 'remark'},
      {data: 'updated_at', name: 'updated_at'},
      {data: 'user_created', name: 'user_created'},
    ]});
  });

  function selection(){
    var request_type = document.getElementById("request_type");
    var application_type = document.getElementById("application_type");
    var requirement_type = document.getElementById("requirement_type");
    var requirement_others = document.getElementById("requirement_others");

    // rq01 == Application
    if(request_type.value == "rq01"){
      application_type.style.display = "block";
    } else {
      application_type.style.display = "none";
    }

    // rt07 == Others
    if(requirement_type.value == "rt07"){
      requirement_others.style.display = "block";
    }else{
      requirement_others.style.display = "none";
    }
  }

  $(document).ready(function(){
    $('.requestor_fullname').change(function(event) {
      $.get('/urf_control', { 'requestor_fullname': $(this).val() },function(data) {
        $('input[name=display_dept]').val(data.department);
        $('input[name=requestor_dept]').val(data.users.department);
        $('input[name=requestor_email]').val(data.users.email);
        $('input[name=requestor_ext]').val(data.users.ext_phone_no);
      });
    });
  });
</script>
@endpush
