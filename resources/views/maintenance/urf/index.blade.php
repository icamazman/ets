@extends('layouts.app_layout')

@section('content')
        <div class="content-wrapper">
          <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Parameter</h4>
                  <p class="card-description">
                    <!-- Add class <code>.table-bordered</code> -->
                  </p>
                  <div class="table-responsive pt-3">
                  <table class="table table-bordered table-striped yajra-datatable">
                      <thead>
                        <tr>
                          <th>
                            
                          </th>
                          <th>
                            URF No.
                          </th>
                          <th>
                            Full Name
                          </th>
                          <th>
                            Request Type
                          </th>
                          <th>
                            Status
                          </th>
                          <th>
                            
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
           </div>
        </div>
@endsection

@push('js')
<script type="text/javascript">
  $(function () {  
    var table = $('.yajra-datatable').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('maintain_urf.index') }}",
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'urf_no', name: 'urf_no'},
      {data: 'fullname', name: 'fullname'},
      {data: 'mergeColumn', name: 'mergeColumn'},
      {data: 'urf_status', name: 'urf_status'},
      {data: 'action', name: 'action', orderable: true, searchable: true},
    ]});
  });
</script>
@endpush