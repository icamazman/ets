@extends('layouts.app_layout')

@hasanyrole('ICT HOU|HOU|Super Admin')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="d-flex justify-content-between">
                            <div class="p-2">
                                <h4 class="card-title">User Request Form (F-001)</h4>
                                <small class="text-muted">
                                Note : Failure to complete this form completely and legibly <br/> may result in delay of processing or the return of your request.<br/>
                                (This section is to be completed by the requester)
                                </small>
                            </div>
                            <div class="p-2">
                                <button class="btn btn-success" data-bs-toggle="modal" data-bs-target=".verify">Verify</button>
                                <button class="btn btn-danger" data-bs-toggle="modal" data-bs-target=".reject">Reject</button>
                                <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target=".modal-trackinglog">Tracking Log</button>
                            </div>
                        </div>
                    </div>

                    <div class="form-sample">
                        <hr>
                        <p class="card-description">Requester Info</p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>User Full Name</label>
                                    <div class="align-self-center">
                                        <input type="text" class="form-control" name="user_fullname" value="{{ $tracking->users->fullname }}" disabled/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Department/Sect</label>
                                    <div class="align-self-center">
                                        <input type="text" class="form-control" name="user_dept" value="{{ $tracking->departments->param_desc }}" disabled/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>E-Mail Address</label>
                                    <div class="align-self-center">
                                        <input type="email" class="form-control" name="user_email" value="{{ $tracking->user_email }}" disabled/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ext. Phone No.</label>
                                    <div class="align-self-center">
                                        <input type="text" class="form-control" name="user_ext" value="{{ $tracking->user_ext }}" disabled/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="card-description"><input type="checkbox" class="form-check-input mx-2" id="requestonbehalf" {{ $tracking->requestor_fullname == 0 ? '' : 'checked' }}>Request on behalf</p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Requester Name</label>
                                    <div class="align-self-center" >
                                        @if($tracking->requestor_fullname == 0)
                                            <input type="text" class="form-control" name="requestor_fullname" value=" "  disabled/>
                                        @else
                                            <input type="text" class="form-control" name="requestor_fullname" value="{{$tracking->req_users->fullname}}" disabled/>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Department/Sect</label>
                                    <div class="align-self-center">
                                        @if($tracking->requestor_dept == 0)
                                            <input type="text" class="form-control" name="requestor_dept" value=" "  disabled/>
                                        @else
                                            <input type="text" class="form-control" name="requestor_dept" value="{{$tracking->req_departments->param_desc}}" disabled/>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>E-Mail Address</label>
                                    <div class="align-self-center">
                                        @if($tracking->requestor_email == 0)
                                            <input type="text" class="form-control" name="requestor_email" value=" "  disabled/>
                                        @else
                                            <input type="text" class="form-control" name="requestor_email" value="{{$tracking->requestor_email}}" disabled/>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ext. Phone No.</label>
                                    <div class="align-self-center">
                                        @if($tracking->requestor_ext == 0)
                                            <input type="text" class="form-control" name="requestor_ext" value=" "  disabled/>
                                        @else
                                            <input type="text" class="form-control" name="requestor_ext" value="{{$tracking->requestor_ext}}" disabled/>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="card-description">Request Details</p>
                        <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                                <label>Request Type</label>
                                <div class="align-self-center">
                                <select class="form-control" name="request_type" disabled>
                                    <option value="{{ $tracking->request_type }}" {{ $tracking->request_type == 0 ? '' : 'selected'  }}>{{ $tracking->request_types->param_desc }}</option>
                                </select>
                                </div>
                            </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-group">
                                <label>Requirement Type</label>
                                <div class="align-self-center">
                                <select class="form-control" name="requirement_type" disabled>
                                    <option value="{{ $tracking->requirement_type }}" {{ $tracking->requirement_type == 0 ? '' : 'selected'  }}>{{ $tracking->requirement_types->param_desc }}</option>
                                    <option>New</option>
                                    <option>Change</option>
                                    <option>Data Extraction</option>
                                    <option>Others</option>
                                </select>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                            @if($tracking->request_type == "rq01")
                            <div class="form-group">
                                <label>Application</label>
                                <div class="align-self-center">
                                <select class="form-control" name="application" disabled>
                                <option value="{{ $tracking->application }}" {{ $tracking->application == 0 ? '' : 'selected'  }}>{{ $tracking->applications->param_desc }}</option>
                                </select>
                                </div>
                            </div>
                            @endif
                            </div>
                            <div class="col-md-6">
                            @if(boolval($tracking->others) == 1)
                            <div class="form-group">
                                <label>Requirement Type: Others</label>
                                <div class="align-self-center">
                                <input type="text" class="form-control" name="others" value="{{ $tracking->others }}" disabled/>
                                </div>
                            </div>
                            @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label>Criticality</label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="criticality" id="membershipRadios1" value="low" {{$tracking->criticality == "c01" ? 'checked' : 'disabled'}}>
                                            Low
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="criticality" id="membershipRadios2" value="medium" {{$tracking->criticality == "c02" ? 'checked' : 'disabled'}}>
                                                Medium
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="criticality" id="membershipRadios1" value="high" {{$tracking->criticality == "c03" ? 'checked' : 'disabled'}}>
                                            High
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Attachment</label>
                                    <button class="btn btn-primary btn-sm p-1" data-bs-toggle="modal" data-bs-target=".download">View</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleTextarea1">Description</label>
                                    <textarea class="form-control-lg col-md-12" id="exampleTextarea1" rows="10" name="description" disabled>{{ $tracking->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" id="completiondate" {{ $tracking->required_completion_date == 0 ? '' : 'checked' }}>
                                    Required Completion Date
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" id="ownerapproval" {{ $tracking->required_owner_approval == 0 ? '' : 'checked' }}>
                                    Required Owner Approval
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <div class="align-self-center" id="completiondateform">
                                    <input type="text" class="form-control" name="required_completion_date" value="{{$tracking->required_completion_date}}" disabled/>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="align-self-center" id="ownerapprovalform">
                                    <select class="form-control" name="required_owner_approval">
                                        <option value="{{ $tracking->required_owner_approval }}" {{ $tracking->required_owner_approval == 0 ? '' : 'selected'  }}>
                                        @if($tracking->required_owner_approval == 0) @else {{$tracking->owners->param_desc}} @endif
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form method="POST" action="{{ route('staff_verify.update', $tracking->id) }}" >
    @method('put')
    @csrf
    @include('modal/staff_verify/verify')
</form>
@include('modal/staff_reject/HOU')
@include('modal/trackinglog/trackinglog')
@include('modal/download_file/download')

@endsection

@else

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <h1 style="text-align: center" class="text-black-50">Sorry...</h1>
            <h2 style="text-align: center">You are not authorized to this page.</h2>
        </div>
    </div>
@endsection

@endhasanyrole

@push('js')
<script type="text/javascript">
    $(function () {
        var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('staff_verify.edit', $tracking->id) }}",
        columns: [
        {data: 'DT_RowIndex', name: 'DT_RowIndex'},
        {data: 'urf_no', name: 'urf_no'},
        {data: 'urf_status', name: 'urf_status'},
        {data: 'remark', name: 'remark'},
        {data: 'updated_at', name: 'updated_at'},
        {data: 'user_created', name: 'user_created'},
        ]});
    });


</script>
@endpush
