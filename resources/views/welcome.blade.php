<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>PUNB e-Ticketing System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .center {
                position: absolute;
                left: 50%;
                margin: -25px 0 0 -25px;
                top: 50%;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > button > a {
                color: black;
                border-radius: 100px;
                padding: 10px 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                margin-top: 5%;
            }

            .links > a:hover, button:hover, a:focus, button:focus, a:active, button:active {
                color: white;
                text-decoration: none;
            }

            .links > button {
                background-color: #ff6088; /* Green */
                border: none;
                border-radius: 150px;
                color: black;
                padding: 10px 25px;
                -webkit-transition-duration: 0.4s;
                transition-duration: 0.4s;
                margin-top: 0%;
            }

            .links > button:hover {
                box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);

            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .animation{
                -webkit-animation-duration: 1s;
                animation-duration: 1s;
                -webkit-animation-fill-mode: both;
                animation-fill-mode: both;
            }

            @-webkit-keyframes fadeInDown {
                0% {
                    opacity: 0;
                    -webkit-transform: translateY(-20px);
                }
                100% {
                    opacity: 1;
                    -webkit-transform: translateY(0);
                }
            }

            @keyframes fadeInDown {
                0% {
                    opacity: 0;
                    transform: translateY(-20px);
                }
                100% {
                    opacity: 1;
                    transform: translateY(0);
                }
            }

            .fadeInDown {
                -webkit-animation-name: fadeInDown;
                animation-name: fadeInDown;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height animation fadeInDown">
            <div class="content">
                <img src=" {{ asset('images/logo.png') }} " alt="">
                <br>
                <h1>e-Ticketing System</h1>
                @if (Route::has('login'))
                    <div class="flex-center links">
                        @auth
                            <button><a href="{{ url('/home') }}">Home</a></button>
                        @else
                            <button><a href="{{ route('login') }}">Login</a></button>
                            <button style="margin-left: 5%"><a href="{{ route('registerStaff') }}">Register</a></button>
                        @endauth
                    </div>
                @endif

            </div>
        </div>
    </body>
</html>
