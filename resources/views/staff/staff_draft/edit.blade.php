@extends('layouts.app_layout')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-10">
              <h4 class="card-title">User Request Form (F-001)</h4>
              <small class="text-muted">
                Note : Failure to complete this form completely and legibly may result in delay of processing or the return of your request.
                <br> (This section is to be completed by the requester)
              </small>
            </div>
            <div class="col-md-2">
              <button class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Tracking Log</button>
            </div>
          </div>
          <form class="form-sample" name="urf_store" action="{{ route('urf_control.store') }}" method="POST">
            @csrf<hr>
            <p class="card-description">Requester Info</p>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>User Full Name</label>
                  <div class="align-self-center" >
                    <input type="text" class="form-control" name="user_fullname" value="{{ $tracking->users->fullname }}" disabled/>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Department/Sect</label>
                  <div class="align-self-center">
                    <input type="text" class="form-control" name="user_dept" value="{{ $tracking->departments->param_desc }}" disabled/>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>E-Mail Address</label>
                  <div class="align-self-center">
                    <input type="email" class="form-control" name="user_email" value="{{ $tracking->user_email }}" disabled/>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Ext. Phone No.</label>
                  <div class="align-self-center">
                    <input type="text" class="form-control" name="user_ext" value="{{ $tracking->user_ext }}" disabled/>
                  </div>
                </div>
              </div>
            </div>

              <p class="card-description"><input type="checkbox" class="form-check-input mx-2" id="requestonbehalf" {{ $tracking->requestor_fullname == 0 ? '' : 'checked' }}>Request on behalf</p>
              <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Requester Name</label>
                          <div class="align-self-center" >
                              @if($tracking->requestor_fullname == 0)
                                  <input type="text" class="form-control" name="requestor_fullname" value=" "  disabled/>
                              @else
                                  <input type="text" class="form-control" name="requestor_fullname" value="{{$tracking->req_users->fullname}}" disabled/>
                              @endif
                          </div>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Department/Sect</label>
                          <div class="align-self-center">
                              @if($tracking->requestor_dept == 0)
                                  <input type="text" class="form-control" name="requestor_dept" value=" "  disabled/>
                              @else
                                  <input type="text" class="form-control" name="requestor_dept" value="{{$tracking->req_departments->param_desc}}" disabled/>
                              @endif
                          </div>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                          <label>E-Mail Address</label>
                          <div class="align-self-center">
                              @if($tracking->requestor_email == 0)
                                  <input type="text" class="form-control" name="requestor_email" value=" "  disabled/>
                              @else
                                  <input type="text" class="form-control" name="requestor_email" value="{{$tracking->requestor_email}}" disabled/>
                              @endif
                          </div>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label>Ext. Phone No.</label>
                          <div class="align-self-center">
                              @if($tracking->requestor_ext == 0)
                                  <input type="text" class="form-control" name="requestor_ext" value=" "  disabled/>
                              @else
                                  <input type="text" class="form-control" name="requestor_ext" value="{{$tracking->requestor_ext}}" disabled/>
                              @endif
                          </div>
                      </div>
                  </div>
              </div>


              <p class="card-description">Request Details</p>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Request Type</label>
                    <div class="align-self-center">
                      <select class="form-control" name="request_type" disabled>
                        <option value="{{ $tracking->request_type }}" {{ $tracking->request_type == 0 ? '' : 'selected'  }}>{{ $tracking->request_types->param_desc }}</option>
                        <option>Application</option>
                        <option>Hardware</option>
                        <option>Others</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Requirement Type</label>
                    <div class="align-self-center">
                      <select class="form-control" name="requirement_type" disabled>
                        <option value="{{ $tracking->requirement_type }}" {{ $tracking->requirement_type == 0 ? '' : 'selected'  }}>{{ $tracking->requirement_types->param_desc }}</option>
                        <option>New</option>
                        <option>Change</option>
                        <option>Data Extraction</option>
                        <option>Others</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  @if($tracking->request_type == "rq01")
                  <div class="form-group">
                    <label>Application</label>
                    <div class="align-self-center">
                      <select class="form-control" name="application" disabled>
                      <option value="{{ $tracking->application }}" {{ $tracking->application == 0 ? '' : 'selected'  }}>{{ $tracking->applications->param_desc }}</option>
                      <option value="IMAS">IMAS - INVESTMENT MONITORING APPLICATION SYSTEM</option>
                      <option value="FMS">FMS - FINANCIAL MANAGEMENT SYSTEM</option>
                      <option value="CODA">CODA - CODA FINANCIAL</option>
                      <option value="EMAIL">EMAIL - </option>
                      <option value="LMS">LMS - LITIGIATION MANAGEMENT SYSTEM</option>
                      <option value="ZCMS">ZCMS - ZAKAT & CHARITY MANAGEMENT SYSTEM</option>
                      <option value="RRS">RRS - ROOM RESERVATION SYSTEM</option>
                      <option value="EDMS">EDMS - ELECTRONIC DOCUMENT MANAGEMENT SYSTEM</option>
                      <option value="TAS">TRAINING APPLICATION SYSTEM</option>
                      <option value="ELEAVE">E-LEAVE APPLICATION SYSTEM</option>
                      <option value="TMS">TMS - TENANCY MANAGEMENT SYSTEM</option>
                      <option value="SMSBLAST">SMSBLAST - </option>
                      <option value="CCMS">CCMS- </option>
                      <option value="Q-RADAR">Q-RADAR - </option>
                      <option value="BRF">BRF - BUMIPUTERA RELIEF FINANCIAL</option>
                      <option value="QLIK">QLIK - </option>
                      <option value="TRAMS">TRAMS - </option>
                      <option value="HRMS">HRMS - </option>
                      <option value="ACL">ACL - AUDIT COMMAND LANGUAGE</option>
                      <option value="PUSTAKA">PUSTAKA - </option>
                      </select>
                    </div>
                  </div>
                  @endif
                </div>
                <div class="col-md-6">
                  @if(boolval($tracking->others) == 1)
                  <div class="form-group">
                    <label>Requirement Type: Others</label>
                    <div class="align-self-center">
                      <input type="text" class="form-control" name="others" value="{{ $tracking->others }}" disabled/>
                    </div>
                  </div>
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group row">
                    <label>Criticality</label>
                    <div class="col-sm-4">
                      <div class="form-check">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="criticality" id="membershipRadios1" value="low" {{$tracking->criticality == "c01" ? 'checked' : ''}} >
                          Low
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-check">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="criticality" id="membershipRadios2" value="medium" {{$tracking->criticality == "c02" ? 'checked' : ''}}>
                            Medium
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-check">
                        <label class="form-check-label">
                          <input type="radio" class="form-check-input" name="criticality" id="membershipRadios1" value="high" {{$tracking->criticality == "c03" ? 'checked' : ''}}>
                          High
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="form-group">
                    <label for="exampleTextarea1">Description</label>
                      <textarea class="form-control-lg col-md-12" id="exampleTextarea1" rows="10" name="description" value="{{ $tracking->description }}" disabled></textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-check form-check-flat form-check-primary">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" id="completiondate" {{ $tracking->required_completion_date == 0 ? '' : 'checked' }}>
                      Required Completion Date
                    </label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-check form-check-flat form-check-primary">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" id="ownerapproval" {{ $tracking->required_owner_approval == 0 ? '' : 'checked' }}>
                      Required Owner Approval
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 mb-3">
                  <div class="align-self-center" id="completiondateform">
                    <input type="text" class="form-control" name="required_completion_date" value="{{$tracking->required_completion_date}}"/>
                  </div>
                </div>
                <div class="col-md-6 mb-3">
                  <div class="align-self-center" id="ownerapprovalform">
                    <select class="form-control" name="required_owner_approval">
                    <option value="{{ $tracking->required_owner_approval }}" {{ $tracking->required_owner_approval == 0 ? '' : 'selected'  }}>{{ $tracking->required_owner_approval }}</option>
                      <option>Application</option>
                      <option>Hardware</option>
                      <option>Others</option>
                    </select>
                  </div>
                </div>
              </div>
            <input type="hidden" class="form-control" name="user_created" value="{{ Auth::user()->id }}"/>
            <input type="hidden" class="form-control" name="user_lastmaintain" value="{{ Auth::user()->id }}"/>
            <input type="hidden" class="form-control" name="urf_status" value="1"/>
            <input type="hidden" class="form-control" name="urf_no" value="1"/>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped yajra-datatable">
                <thead>
                    <tr>
                    <th>

                    </th>
                    <th>
                        URF No.
                    </th>
                    <th>
                        URF Status
                    </th>
                    <th>
                        Remark
                    </th>
                    <th>
                        Last Maintain
                    </th>
                    <th>
                        Created By
                    </th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection
