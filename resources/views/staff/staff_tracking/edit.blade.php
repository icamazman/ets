@extends('layouts.app_layout')



@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="d-flex justify-content-between">
                            <div class="p-2">
                                <h4 class="card-title">User Request Form (F-001)</h4>
                                <small class="text-muted">
                                Note : Failure to complete this form completely and legibly <br/>
                                may result in delay of processing or the return of your request.<br/>
                                (This section is to be completed by the requester)
                                </small>
                            </div>
                            <div class="p-2">
                                @if (Auth::user()->id == $tracking->user_fullname)
                                    @if($tracking->urf_status == "stat08")
                                    <button class="btn btn-warning" data-bs-toggle="modal" data-bs-target=".sendrequestowner">Send Request Owner</button>
                                    @endif

                                    @if ($tracking->urf_status == "stat14")
                                    <button class="btn btn-success" data-bs-toggle="modal" data-bs-target=".accept">Accept</button>
                                    @endif
                                @endif
                                <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target=".modal-trackinglog">Tracking Log</button>
                            </div>
                        </div>
                    <form class="form-sample" action="{{ route('staff_tracking.update', $tracking->id) }}" method="POST" enctype="multipart/form-data">
                        @method('put')
                        @csrf
                        <p class="card-description">Requester Info</p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>User Full Name</label>
                                    <div class="align-self-center" >
                                        <input type="text" class="form-control" name="user_fullname" value="{{ $tracking->users->fullname }}" disabled/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Department/Sect</label>
                                    <div class="align-self-center">
                                        <input type="text" class="form-control" name="user_dept" value="{{ $tracking->departments->param_desc }}" disabled/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>E-Mail Address</label>
                                    <div class="align-self-center">
                                        <input type="email" class="form-control" name="user_email" value="{{ $tracking->user_email }}" disabled/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ext. Phone No.</label>
                                    <div class="align-self-center">
                                        <input type="text" class="form-control" name="user_ext" value="{{ $tracking->user_ext }}" disabled/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if($tracking->requestor_fullname)
                        <p class="card-description"><input type="checkbox" class="form-check-input mx-2" id="requestonbehalf" {{ $tracking->requestor_fullname == 0 ? '' : 'checked' }}>Request on behalf</p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Requester Name</label>
                                    <div class="align-self-center" >
                                        @if(!$tracking->requestor_fullname)
                                            <input type="text" class="form-control" name="requestor_fullname" value=" "  disabled/>
                                        @else
                                            <input type="text" class="form-control" name="requestor_fullname" value="{{$tracking->req_users->fullname}}" disabled/>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Department/Sect</label>
                                    <div class="align-self-center">
                                        @if(!$tracking->requestor_dept)
                                            <input type="text" class="form-control" name="requestor_dept" value=" "  disabled/>
                                        @else
                                            <input type="text" class="form-control" name="requestor_dept" value="{{$tracking->req_departments->param_desc}}" disabled/>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>E-Mail Address</label>
                                    <div class="align-self-center">
                                        @if(!$tracking->requestor_email)
                                            <input type="text" class="form-control" name="requestor_email" value=" "  disabled/>
                                        @else
                                            <input type="text" class="form-control" name="requestor_email" value="{{$tracking->requestor_email}}" disabled/>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ext. Phone No.</label>
                                    <div class="align-self-center">
                                        @if(!$tracking->requestor_ext)
                                            <input type="text" class="form-control" name="requestor_ext" value=" "  disabled/>
                                        @else
                                            <input type="text" class="form-control" name="requestor_ext" value="{{$tracking->requestor_ext}}" disabled/>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                        <p class="card-description">Request Details</p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Request Type</label>
                                    <div class="align-self-center">
                                    <select class="form-control" name="request_type" {{ $tracking->urf_status == "stat03" ? '' : 'disabled' }}>
                                        @if ($tracking->urf_status == "stat03")
                                        <option value="{{ $tracking->request_type }}" selected>{{ $tracking->request_types->param_desc }} - Selected</option>
                                            @foreach($request_type as $id => $value)
                                            <option value="{{$id}}">{{$value}}</option>
                                            @endforeach
                                        @else
                                        <option value="{{ $tracking->request_type }}" {{ $tracking->request_type == 0 ? '' : 'selected'  }}>{{ $tracking->request_types->param_desc }}</option>
                                        @endif
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Requirement Type</label>
                                    <div class="align-self-center">
                                    <select class="form-control" name="requirement_type" {{ $tracking->urf_status == "stat03" ? '' : 'disabled' }}>
                                        @if ($tracking->urf_status == "stat03")
                                        <option value="{{ $tracking->requirement_type }}" selected>{{ $tracking->requirement_types->param_desc }} - Selected</option>
                                        @foreach($requirement_type as $id => $value)
                                        <option value="{{$id}}">{{$value}}</option>
                                        @endforeach
                                        @else
                                        <option value="{{ $tracking->requirement_type }}" {{ $tracking->requirement_type == 0 ? '' : 'selected'  }}>{{ $tracking->requirement_types->param_desc }}</option>
                                        @endif
                                    </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                @if($tracking->request_type == "rq01")
                                <div class="form-group" {{ $tracking->request_type == "rq01" ? '' : 'style=display:none;'}}>
                                    <label>Application</label>
                                    <div class="align-self-center" >
                                        <select class="form-control" name="application" {{ $tracking->urf_status == "stat03" ? '' : 'disabled' }}>
                                            @if ($tracking->urf_status == "stat03")
                                            <option value="{{ $tracking->application }}" selected>{{  $tracking->applications->param_desc }} - Selected</option>
                                            @foreach($applications as $id => $application)
                                            <option value="{{$id}}">{{$application}}</option>
                                            @endforeach
                                            @else
                                            <option value="{{ $tracking->application }}" {{ $tracking->application == 0 ? '' : 'selected'  }}>{{  $tracking->applications->param_desc }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                @if(boolval($tracking->others) == 1)
                                <div class="form-group">
                                    <label>Requirement Type: Others</label>
                                    <div class="align-self-center">
                                        <input type="text" class="form-control" name="others" value="{{ $tracking->others }}" {{ $tracking->urf_status == "stat03" ? '' : 'disabled' }}/>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label>Criticality</label>
                                    @if ($tracking->urf_status == "stat03")
                                    @foreach($critically as $id => $value)
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="criticality" id="membershipRadios1" value="{{$id}}"
                                            @if($tracking->criticality == $id) checked @endif>
                                            {{$value}}
                                        </label>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="criticality" id="membershipRadios1" value="low" {{ $tracking->criticality == "c01" ? 'checked' : 'disabled'}} >
                                            Low
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="criticality" id="membershipRadios2" value="medium" {{$tracking->criticality == "c02" ? 'checked' : 'disabled'}}>
                                                Medium
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="criticality" id="membershipRadios1" value="high" {{$tracking->criticality == "c03" ? 'checked' : 'disabled'}}>
                                            High
                                            </label>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Attachment</label>
                                    @if ($tracking->urf_status == "stat03")
                                    <input class="form-control form-control-sm mb-2" id="formFileSm" type="file" name="file_path"/>
                                    @endif
                                    <a class="btn btn-primary btn-sm p-1" data-bs-toggle="modal" data-bs-target=".download">View</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleTextarea1">Description</label>
                                    <textarea class="form-control-lg col-md-12" id="exampleTextarea1" rows="10" name="description" {{ $tracking->urf_status == "stat03" ? '' : 'disabled' }}>{{ $tracking->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" id="completiondate" {{ $tracking->required_completion_date == 0 ? '' : 'checked' }} {{ $tracking->urf_status == "stat03" ? '' : 'disabled' }}>
                                        Required Completion Date
                                        </label>
                                </div>
                            </div>
                            @if($tracking->urf_status == "stat08")
                            <div class="col-md-6">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" id="ownerapproval" {{ $tracking->urf_status == "stat08" ? 'checked' : '' }} {{ $tracking->required_owner_approval == 0 ? '' : 'checked' }}>
                                    Required Owner Approval
                                    </label>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <div class="align-self-center" id="completiondateform">
                                    @if ($tracking->urf_status == "stat03")
                                    <input type="date" class="form-control" name="required_completion_date" value="{{old('required_completion_date')}}" />
                                    @else
                                    <input type="text" class="form-control" name="required_completion_date" value="{{$tracking->required_completion_date}}" {{ $tracking->urf_status == "stat03" ? '' : 'disabled' }}/>
                                    @endif
                                </div>
                            </div>
                            @if($tracking->urf_status == "stat08")
                            <div class="col-md-6 mb-3">
                                <div class="align-self-center" id="ownerapprovalform">
                                    <select class="form-control" name="required_owner_approval">
                                        @foreach($department as $id => $department)
                                            <option value="{{$id}}">{{$department}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                        </div>
                        <input type="hidden" class="form-control" name="user_created" value="{{ Auth::user()->fullname }}"/>
                        <input type="hidden" class="form-control" name="user_lastmaintain" value="{{ Auth::user()->fullname }}"/>
                        <input type="hidden" class="form-control" name="urf_no" value="{{ $tracking->id }}"/>
                        <input type="hidden" class="form-control" name="urf_status" id="status"/>
                        @include('modal/staff_submit/resubmit')
                        @include('modal/staff_requestowner/requestowner')
                        @include('modal/user_accept/accept')
                        @include('modal/user_accept/incomplete')
                    </form>
                    @if ($tracking->urf_status == "stat03")
                    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                        <button class="btn btn-success" data-bs-toggle="modal" data-bs-target=".resubmit">Re-submit</button>
                    </div>
                    @endif
                    @if( in_array($tracking->urf_status, array('stat09', 'stat12', 'stat14', 'stat15', 'stat16', 'stat17', 'stat20') ) )
                        <h4>ICT Side</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Requirement Type</label>
                                    <div class="align-self-center">
                                    {{-- <select class="form-control" name="requirement_type_it">
                                        <option value="{{ $tracking->requirement_type_it }}" {{ $tracking->requirement_type_it == 0 ? '' : 'selected disabled' }}>
                                            @if($tracking->requirement_type_it == 0) @else {{$tracking->requirement_types_it->param_desc}} @endif
                                        </option>
                                    </select> --}}
                                    <input type="text" class="form-control" name="requirement_type_it" value="{{ $tracking->requirement_types_it->param_desc }}" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                @if(boolval($tracking->others_it) == 1)
                                <div class="form-group">
                                    <label>Requirement Type: Others</label>
                                    <div class="align-self-center">
                                        <input type="text" class="form-control" name="others" value="{{ $tracking->others_it }}" readonly/>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label>Complexity</label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="complexity" id="membershipRadios1" value="co01" {{$tracking->complexity == "co01" ? 'checked' : 'disabled'}}>
                                            Low
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="complexity" id="membershipRadios2" value="co02" {{$tracking->complexity == "co02" ? 'checked' : 'disabled'}}>
                                                Medium
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="complexity" id="membershipRadios1" value="co03" {{$tracking->complexity == "co03" ? 'checked' : 'disabled'}}>
                                            High
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label> ICTD Testing Date:</label>
                                    <input type="text" class="form-control" name="dt_ictd_testing" value="{{ $tracking->dt_ictd_testing }}" readonly/>
                                </div>
                                <div class="col-md-6">
                                    <label>User Testing Date:</label>
                                    <input type="text" class="form-control" name="dt_user_testing" value="{{ $tracking->dt_user_testing }}" readonly/>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>


@include('modal/trackinglog/trackinglog')
@include('modal/download_file/download')

@endsection

{{-- @else --}}

{{-- @section('content')
    <div class="content-wrapper">
        <div class="row">
            <h1 style="text-align: center" class="text-black-50">Sorry...</h1>
            <h2 style="text-align: center">You are not authorized to this page.</h2>
        </div>
    </div>
@endsection --}}




@push('js')
<script type="text/javascript">
  $(function () {
    var table = $('.yajra-datatable').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('staff_tracking.edit', $tracking->id) }}",
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'urf_no', name: 'urf_no'},
      {data: 'urf_status', name: 'urf_status'},
      {data: 'remark', name: 'remark'},
      {data: 'updated_at', name: 'updated_at'},
      {data: 'user_created', name: 'user_created'},
    ]});
  });

    if($('button[name=completerequest]').click(function(){
      $('input[name=urf_status]').val('stat16');
    }));

    if($('button[name=incompleterequest]').click(function(){
      $('input[name=urf_status]').val('stat15');
    }));

    if($('button[name=requestowner]').click(function(){
      $('input[name=urf_status]').val('stat18');
    }));

    if($('button[name=resubmit]').click(function(){
      $('input[name=urf_status]').val('stat13');
    }));


</script>
@endpush
