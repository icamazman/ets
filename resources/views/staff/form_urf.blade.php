@extends('layouts.app_layout')

@section('content')

<div class="content-wrapper">
    <div class="row">
        <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
            <h4 class="card-title">User Request Form (F-001)</h4>
            <small class="text-muted">
            Note : Failure to complete this form completely and legibly may result in delay of processing or the return of your request.
            <br> (This section is to be completed by the <span style="font-weight: 900;">requester</span>)
            </small>
            <form class="form-sample" name="urf_store" action="{{ route('urf_control.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <hr>
                <p class="card-description">Requester Info</p>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label>User Fullname</label>
                    <div class="align-self-center" >
                        <input type="text" class="form-control" name="user_fullname" value="{{auth()->user()->fullname}}" disabled>
                        <input type="hidden" class="form-control" name="user_fullname" value="{{auth()->user()->id}}">
                    </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label>Department/Section</label>
                    <div class="align-self-center">
                        <input type="text" class="form-control" value="{{auth()->user()->departments->param_desc}}" disabled>
                        <input type="hidden" class="form-control" name="user_dept" value="{{auth()->user()->department}}">
                    </div>
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label>E-mail Address</label>
                    <div class="align-self-center">
                        <input type="email" class="form-control" value="{{auth()->user()->email}}" disabled>
                        <input type="hidden" class="form-control" name="user_email" value="{{auth()->user()->email}}">
                    </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label>Ext. Phone No.</label>
                    <div class="align-self-center">
                        <input type="text" class="form-control" value="{{auth()->user()->ext_phone_no}}" disabled>
                        <input type="hidden" class="form-control" name="user_ext" value="{{auth()->user()->ext_phone_no}}">
                    </div>
                    </div>
                </div>
                </div>
                <p class="card-description">
                <input type="checkbox" class="form-check-input mx-2" id="requestonbehalf" value="selected" name="requestonbehalf">
                <label for="requestonbehalf">Request on behalf</label>
                </p>
                <div class="input-on-behalf" style="display: none;">
                <div class="row">
                    <div class="col-md-6" id="requestonbehalfform1">
                    <div class="form-group">
                        <label>Requester Name</label>
                        <div class="align-self-center" >
                        {{-- <input type="text" class="form-control" name="requestor_fullname"/> --}}
                        <select name="requestor_fullname" class="form-control requestor_fullname">
                            <option value="" selected disabled>Select name</option>
                            @foreach($users as $user)
                            <option value="{{$user->id}}" @if($user->id == old('requestor_fullname')) selected @endif>{{$user->fullname}}</option>
                            @endforeach
                        </select>
                        @error('requestor_fullname')
                            <div class="text-danger small">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6" id="requestonbehalfform2">
                    <div class="form-group">
                        <label>Department/Sect</label>
                        <div class="align-self-center">
                        <input type="text" class="form-control" name="display_dept" disabled>
                        <input type="hidden" class="form-control" name="requestor_dept"/>
                        </div>
                        @error('requestor_dept')
                        <div class="text-danger small">{{ $message }}</div>
                        @enderror
                    </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6" id="requestonbehalfform3">
                    <div class="form-group">
                        <label>E-Mail Address</label>
                        <div class="align-self-center">
                        <input type="email" class="form-control" name="requestor_email" disabled>
                        <input type="hidden" class="form-control" name="requestor_email"/>
                        </div>
                        @error('requestor_email')
                        <div class="text-danger small">{{ $message }}</div>
                        @enderror
                    </div>
                    </div>

                    <div class="col-md-6" id="requestonbehalfform4">
                    <div class="form-group">
                        <label>Ext. Phone No.</label>
                        <div class="align-self-center">
                        <input type="text" class="form-control" name="requestor_ext" disabled/>
                        <input type="hidden" class="form-control" name="requestor_ext"/>
                        </div>
                        @error('requestor_ext')
                        <div class="text-danger small">{{ $message }}</div>
                        @enderror
                    </div>
                    </div>
                </div>
                </div>

                <p class="card-description">Request Details</p>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label>Request Type</label>
                    <div class="align-self-center">
                        <select class="form-control" name="request_type" id="request_type" onchange="selection();">
                        <option value="" selected disabled>Select Request Type</option>
                        @foreach($request_type as $id => $value)
                        <option value="{{$id}}">{{$value}}</option>
                        @endforeach
                        </select>
                    </div>
                    @error('request_type')
                        <div class="text-danger small">{{ $message }}</div>
                    @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label>Requirement Type</label>
                    <div class="align-self-center">
                        <select class="form-control" name="requirement_type" id="requirement_type" onchange="selection();">
                        <option selected value="" disabled>Select one requirement type</option>
                        @foreach($requirement_type as $id => $value)
                        <option value="{{$id}}">{{$value}}</option>
                        @endforeach
                        </select>
                    </div>
                    @error('requirement_type')
                        <div class="text-danger small">{{ $message }}</div>
                    @enderror
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group" id="application_type" style="display: none;">
                    <label>Application</label>
                    <div class="align-self-center" >
                        <select class="form-control" name="application">
                        <option disabled selected>Select one application</option>
                        @foreach($applications as $id => $application)
                        <option value="{{$id}}">{{$application}}</option>
                        @endforeach
                        </select>
                    </div>
                    @error('application')
                        <div class="text-danger small">{{ $message }}</div>
                    @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" id="requirement_others" style="display: none;">
                    <label>Requirement Type: Others</label>
                    <div class="align-self-center">
                        <input type="text" class="form-control" name="others"/>
                    </div>
                    @error('requirement_others')
                        <div class="text-danger small">{{ $message }}</div>
                    @enderror
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                    <div class="form-group row">
                    <label>Criticality</label>
                        @foreach($critically as $id => $value)
                        <div class="col-sm-4">
                            <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="criticality" id="membershipRadios1" value="{{$id}}"
                                @if(old('criticality') === $id) checked @endif>
                                {{$value}}
                            </label>
                            </div>
                        </div>
                        @endforeach

                        @error('criticality')
                        <div class="text-danger small">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Attachment</label>
                            <input class="form-control form-control-sm" id="formFileSm" type="file" name="file_path"/>
                            @error('file_path')
                                <div class="text-danger small">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                        <label for="exampleTextarea1">Description</label>
                        <textarea class="form-control-lg col-md-12" id="exampleTextarea1" rows="10" name="description">{{old('description')}}</textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-check form-check-flat form-check-primary">
                        <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" id="completiondate" name="completiondate" value="selected">
                            Required Completion Date
                        </label>
                        </div>
                    </div>
                    {{-- <div class="col-md-6">
                        <div class="form-check form-check-flat form-check-primary">
                        <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" id="ownerapproval" name="ownerapproval" value="selected">
                        Required Owner Approval
                        </label>
                        </div>
                    </div> --}}
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="align-self-center" id="completiondateform" style="display: none;">
                        <input type="date" class="form-control" name="required_completion_date" value="{{old('required_completion_date')}}" />
                        </div>
                        @error('required_completion_date')
                        <div class="text-danger small">{{ $message }}</div>
                        @enderror
                    </div>
                    {{-- <div class="col-md-6">
                        <div class="align-self-center" id="ownerapprovalform" style="display: none;">
                            <select class="form-control" name="required_owner_approval" id="roa">
                            <option disabled selected>Select one department</option>
                            @foreach($department as $id => $department)
                            <option value="{{$id}}">{{$department}}</option>
                            @endforeach
                            </select>
                        </div>
                        @error('required_owner_approval')
                            <div class="text-danger small">{{ $message }}</div>
                        @enderror
                    </div> --}}
                </div>
                <input type="hidden" class="form-control" name="user_created" value="{{ Auth::user()->fullname }}"/>
                <input type="hidden" class="form-control" name="user_lastmaintain" value="{{ Auth::user()->fullname }}"/>
                <input type="hidden" class="form-control" name="urf_status" id="status"/>
                <input type="hidden" class="form-control" name="urf_no" value="0"/>
                @include('modal/staff_submit/sendto')
                @include('modal/staff_submit/submit')
                @include('modal/staff_submit/saveasdrafts')
            </form>
            <div class="d-grid gap-2 d-md-flex justify-content-md-end m-3">
                <button class="btn btn-primary me-2" data-bs-toggle="modal" data-bs-target=".sendto">Submit Form</button>
                <button class="btn btn-light saveasdraft" data-bs-toggle="modal" data-bs-target=".saveasdraft">Save as Draft</button>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>

@include('modal/staff_submit/modalSubmit')
@endsection

@push('css')
<style type="text/css">
    .small {
        font-size: 0.80rem;
    }
</style>
@endpush

@push('js')
<script>
    var myModal = new bootstrap.Modal(document.querySelector('.urf-submit'), {});

    function selection(){
        var request_type = document.getElementById("request_type");
        var application_type = document.getElementById("application_type");
        var requirement_type = document.getElementById("requirement_type");
        var requirement_others = document.getElementById("requirement_others");

        // rq01 == Application
        if(request_type.value == "rq01"){
        application_type.style.display = "block";
        } else {
        application_type.style.display = "none";
        }

        // rt07 == Others
        if(requirement_type.value == "rt07"){
        requirement_others.style.display = "block";
        }else{
        requirement_others.style.display = "none";
        }
    }

    $(document).ready(function(){
        $('.requestor_fullname').change(function(event) {
            $.get('/urf_control', { 'requestor_fullname': $(this).val() },function(data) {
                $('input[name=display_dept]').val(data.department);
                $('input[name=requestor_dept]').val(data.users.department);
                $('input[name=requestor_email]').val(data.users.email);
                $('input[name=requestor_ext]').val(data.users.ext_phone_no);
            });
        });

        if($('.submitform').click(function(e){
            // e.preventDefault();
            var value = $('select[name="sendtoHOU"]').val();
            // console.log(value);
            if(value != ''){
                $('input[name=urf_status]').val('stat01');
            }else{
                $('input[name=urf_status]').val('stat22');
            }
        }));

        if($('.saveasdraft').click(function(){
            $('input[name"urf_status]').val('stat11');
        }));

        $('input[type="checkbox"]').click(function(){
            if($("#requestonbehalf").is(":checked")){
                $(".input-on-behalf").css("display", "block");
                // $(".input-on-behalf").find('input').prop('disabled', false);
                // $(".input-on-behalf").find('select').prop('disabled', false);
                /*$("#requestonbehalfform1").css("display", "block");
                $("#requestonbehalfform2").css("display", "inline");
                $("#requestonbehalfform3").css("display", "inline");
                $("#requestonbehalfform4").css("display", "inline");*/
        } else if($("#requestonbehalf").is(":not(:checked)")){
            $(".input-on-behalf").css("display", "none");
            $(".input-on-behalf").find('input').val('');
            $(".input-on-behalf").find('select').val('');
            /*$("#requestonbehalfform1").css("display", "none");
            $("#requestonbehalfform1").css("display", "none");
            $("#requestonbehalfform2").css("display", "none");
            $("#requestonbehalfform3").css("display", "none");
            $("#requestonbehalfform4").css("display", "none");*/
        }

        if($("#completiondate").is(":checked")){
            $("#completiondateform").css("display", "block");
        } else if($("#completiondate").is(":not(:checked)")){
            $("#completiondateform").css("display", "none");
        }

        if($("#ownerapproval").is(":checked")){
            $("#ownerapprovalform").css("display", "block");
        } else if($("#ownerapproval").is(":not(:checked)")){
            $("#ownerapprovalform").css("display", "none");
        }
        });

        @if(session('msg') && session('status') == "stat01")
            $('.urf-submit .modal-body').html('<span class="text-success">URF successfully has submitted to selected HOU ({{ session("verifier") }}) for HOU Verification and notified them via e-mail. <br><br> You may track your URF at Tracking URF.</span>');
            myModal.show();
        @endif

        @if(session('msg') && session('status') == "stat11")
            $('.urf-submit .modal-body').html('<span class="text-success">URF has been saved as draft. You may check your draft at Draft URF.</span>');
            myModal.show();
        @endif

        @if(session('msg') && session('status') == "stat22")
            $('.urf-submit .modal-body').html('<span class="text-success">URF successfully has submitted to selected HOD ({{ session("approver") }}) for HOD Approver and notified them via e-mail. <br><br> You may track your URF at Tracking URF.</span>');
            myModal.show();
        @endif

        @if($errors->any())
            $('.urf-submit .modal-body').html('<span class="text-danger">URF submission failed. Please check at error message on display.</span>');
            myModal.show();
        @endif
    });
</script>
@endpush
