<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="intro">
        Assalamualaikum and greetings,
        <br>
    </div>
    <div class="body">
        <br>
        @if($ticket_it->urf_status == "stat01") <!-- Pending HOU Verfication -->
            User Request Form For Verification - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('ticket', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat02") <!-- Verified by HOU - Pending HOD Approval -->
            User Request Form For Approval - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('ticket', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat03") <!-- Rejected & Request for Amendment by HOU -->
            User Request Form Incomplete Verification - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please be informed that your request is incompleted. Kindly amend the information as stated in remark section.
            <br><br>
            Please click on the link below to open the document:
            <br>
            <br>
            <a href="{{ route('staff_tracking.edit', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat04") <!-- Approved by HOD - Pending ICT HOU Verification -->
            User Request Form For IT Verfication - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('ticket', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat05") <!-- Rejected & Canceled by HOD -->
            User Request Form Request has been cancelled - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please be informed that your request is cancelled. Kindly refer to the remark section for more cancellation details.
            <br>
            <br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('staff_tracking.edit', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat06") <!-- Verified by IT HOU -->
            User Request Form Verified by IT HOU - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('staff_tracking.edit', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat07") <!-- Rejected & Canceled by IT HOU -->
            User Request Form Request has been cancelled - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please be informed that your request is cancelled. Kindly refer to the remark section for more cancellation details.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('staff_tracking.edit', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat08") <!-- Required Owner Approval -->
            User Request Form Request is required for its owner's approval - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please be informed that your request is incompleted. Kindly amend the information as stated in remark section.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('staff_tracking.edit', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat09") <!-- Approved by IT HOD -->
            @if($ticket_it->request_type == "rq01")
                User Request Form has been approved by IT HOD - {{$ticket_it->users->fullname}} for your reference.
                <br><br>
                (System Enhancement/Project Management)
            @else
                User Request Form has been approved by IT HOD - {{$ticket_it->users->fullname}} for your reference.
                <br><br>
                (Hardware/Others)
            @endif
        @endif

        @if($ticket_it->urf_status == "stat10") <!-- Rejected & Canceled by IT HOD -->
            User Request Form Request has been rejected - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please be informed that your request is cancelled. Kindly refer to the remark section for more cancellation details.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('staff_tracking.edit', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat11") <!-- Save as Draft -->
            User Request Form is saved as draft - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('staff_draft.edit', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat12") <!-- In-Progress -->
            @if($ticket_it->assignTo == "ass01")
                User Request is in-progress - {{$ticket_it->users->fullname}} for your reference.
                <br><br>
                Please be informed that your request has been assigned to {{$ticket_it->officers->fullname}} for above ticket.
            @endif

            @if($ticket_it->assignTo == "ass02")
                User Request is in-progress - {{$ticket_it->users->fullname}} for your reference.
                <br><br>
                Please be informed that your request has been assigned to vendor for above ticket.
            @endif
            <br>
            <br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('ict_pic.edit', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat13") <!-- Resubmit Amended - Pending HOU Verification -->
            User Request Form Amended and Request for Re-verification - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('staff_tracking.edit', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat14") <!-- Pending User Acceptance -->
            User Request Completed - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please be informed that above ticket is completed. Kindly complete the UAT and confirm acceptance within 7 days. Otherwise, the ticket will be auto closed by system.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('staff_tracking.edit', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat15") <!-- Incomplete Request -->
            User Request Incomplete - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please be informed that above ticket is incomplete. Kindly refer to the remark section for the incomplete details.

        @endif

        @if($ticket_it->urf_status == "stat16") <!-- Completed Request -->
            User Request Completed and Closed - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('staff_tracking.edit', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat17") <!-- Pending IT HOD Approval -->
            User Request Form For IT Approval - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('ticket', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat18") <!-- Pending Owner Approval -->
            User Request Form For Owner Approval - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('ticket', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat19") <!-- Approved by Owner -->
            User Request Form has approved by Owner - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('ticket', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat21") <!-- Rejected by Owner -->
            User Request Form has rejected by Owner - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('staff_tracking.edit', $ticket_it->id) }}">[Document Link]</a>
        @endif

        @if($ticket_it->urf_status == "stat22") <!-- Pending HOD Approval -->
            User Request Form For Approval - {{$ticket_it->users->fullname}} for your reference.
            <br><br>
            Please click on the link below to open the document:
            <br><br>
            <a href="{{ route('ticket', $ticket_it->id) }}">[Document Link]</a>
        @endif

    </div>
</body>
</html>
