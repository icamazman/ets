<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table, th, tr, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
    <div class="intro">
        Assalamualaikum and greetings,
        <br>
    </div>
    <div class="body">
        Dear {{ $user->fullname }},
        <br><br>
        Your account is successfully registered.
        <br><br>
        <table>
            <tr>
                <th>Staff No.</th>
                <th>Staff Name</th>
                <th>E-Mail Address</th>
            </tr>
           <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->fullname}}</td>
                <td>{{$user->email}}</td>
           </tr>
        </table>
        <br>
        Click on the link below to activate account: <br>
        <a href="{{ route('verifyStaff', $user->id) }}">Activate Staff Account</a><br><br>
        Please contact ICT Department if you did not perform this registration.
        <br><br>
        Thank you, <br>
        ICTD.
    </div>
</body>
</html>
