<?php

use App\User;
use App\Ticket_IT;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StaffVerifyController;
use Spatie\Permission\Middlewares\RoleMiddleware;
use Spatie\Permission\Middlewares\PermissionMiddleware;
use Spatie\Permission\Middlewares\RoleOrPermissionMiddleware;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/register_staff', function () {
    return view('auth.register_staff');
})->name('registerStaff');

Route::post('/register_staff_email', 'Auth\RegisterController@registerEmail')->name('registerEmail');

Route::get('/verify_staff/{id}', 'Auth\VerificationController@verifyStaff')->name('verifyStaff');

Route::post('/verifying_staff/{id}', 'Auth\VerificationController@verifyingStaff')->name('verifyingStaff');

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('urf_control','URFController')->middleware('auth');
Route::resource('staff_tracking','StaffTrackingController')->middleware('auth');
Route::resource('staff_draft','StaffDraftController')->middleware('auth');
Route::resource('staff_verify','StaffVerifyController')->middleware('auth');
Route::resource('staff_approve','StaffApproveController')->middleware('auth');
Route::resource('ict_verify','ICTVerifyController')->middleware('auth');
Route::resource('ict_approve','ICTApproveController')->middleware('auth');
Route::resource('ict_assign','ICTAssignController')->middleware('auth');
Route::resource('ict_pic','ICTPicController')->middleware('auth');
Route::resource('maintain_param','MaintainParamController')->middleware('auth');
Route::resource('maintain_urf','MaintainURFController')->middleware('auth');
Route::resource('secure_profile','SecureProfileController')->middleware('auth');
Route::resource('secure_email','SecureEmailController')->middleware('auth');
Route::resource('secure_site','SecureSiteController')->middleware('auth');
Route::resource('secure_usersetting','SecureUserSettingController')->middleware('auth');
Route::resource('report_master','ReportMasterController')->middleware('auth');

Route::get('/ticket/{id}', function($id) {
    $ticket = Ticket_IT::find($id);
    // dd(Auth::user()->roles->first()->name);
    if($ticket->urf_status == "stat01"){
        if(in_array(Auth::user()->roles->first()->name, array('ICT HOU', 'HOU', 'Super Admin'))){
            return redirect()->route('staff_verify.edit', $ticket->id);
        }else{
            return redirect()->route('staff_tracking.edit', $ticket->id);
        }
    }

    if($ticket->urf_status == "stat02"){
        if(in_array(Auth::user()->roles->first()->name, array('ICT HOD', 'HOD', 'Super Admin'))){
            return redirect()->route('staff_approve.edit', $ticket->id);
        }else{
            return redirect()->route('staff_tracking.edit', $ticket->id);
        }
    }

    if($ticket->urf_status == "stat04"){
        if(in_array(Auth::user()->roles->first()->name, array('ICT HOU', 'HOU', 'Super Admin'))){
            return redirect()->route('ict_verify.edit', $ticket->id);
        }else{
            return redirect()->route('staff_tracking.edit', $ticket->id);
        }
    }

    if($ticket->urf_status == "stat17"){
        if(in_array(Auth::user()->roles->first()->name, array('ICT HOD', 'Super Admin'))){
            return redirect()->route('ict_approve.edit', $ticket->id);
        }else{
            return redirect()->route('staff_tracking.edit', $ticket->id);
        }
    }

    if($ticket->urf_status == "stat18"){
        if(in_array(Auth::user()->roles->first()->name, array('ICT HOD', 'HOD', 'Super Admin'))){
            return redirect()->route('staff_approve.edit', $ticket->id);
        }else{
            return redirect()->route('staff_tracking.edit', $ticket->id);
        }
    }

    if($ticket->urf_status == "stat19"){
        if(in_array(Auth::user()->roles->first()->name, array('ICT HOU', 'Super Admin'))){
            return redirect()->route('ict_verify.edit', $ticket->id);
        }else{
            return redirect()->route('staff_tracking.edit', $ticket->id);
        }
    }

    if($ticket->urf_status == "stat22"){
        if(in_array(Auth::user()->roles->first()->name, array('ICT HOD', 'HOD', 'Super Admin'))){
            return redirect()->route('staff_approve.edit', $ticket->id);
        }else{
            return redirect()->route('staff_tracking.edit', $ticket->id);
        }
    }

})->name('ticket');
// Route::resource('report','ReportController');

