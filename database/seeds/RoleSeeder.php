<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleid = [
            // 'ro1',
            // 'ro2',
            // 'ro3'
            // 'ro4'
            'ro5'
        ];

        $role_desc = [
            // 'Staff',
            // 'Head of Unit',
            // 'Head of Department'
            // 'Admin'
            'Team Leader'
        ];

        $requestor = [
            // '1',
            // '1',
            // '1'
            '1'
        ];

        $verifier = [
            // '0',
            // '1',
            // '0'
            '0'
        ];

        $approver = [
            // '0',
            // '0',
            '0'
        ];

        $user_created = 'System';

        $user_lastmaintain = 'System';

        for ($i=0; $i < count($roleid); $i++) {
            Role::create([
                'roleid' => $roleid[$i],
                'role_desc' => $role_desc[$i],
                'requestor' => $requestor[$i],
                'verifier' => $verifier[$i],
                'approver' => $approver[$i],
                'user_created' => $user_created,
                'user_lastmaintain' => $user_lastmaintain,
            ]);
        }
        
    }
}
