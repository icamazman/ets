<?php

use App\User;
use Illuminate\Database\Seeder;

class ExtPhoneNoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ext_phone_no = [
            '1937',
            '2198'
        ];

        $username = User::select('username')->get();

        for ($i=0; $i < count($username); $i++) { 
            User::where('id', $i+1)->update([
                'ext_phone_no' => $ext_phone_no[$i],
            ]);
        }
    }
}
