<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnsToTicketIt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_it', function (Blueprint $table) {
            $table->string('requirement_type_it')->nullable();
            $table->string('complexity')->nullable();
            $table->string('dt_ictd_testing')->nullable();
            $table->string('dt_user_testing')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_it', function (Blueprint $table) {
            //
        });
    }
}
