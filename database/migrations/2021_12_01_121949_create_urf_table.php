<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUrfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_it', function (Blueprint $table) {
            $table->id();
            $table->string('urf_no');
            $table->string('user_fullname');
            $table->string('user_dept');
            $table->string('user_email');
            $table->string('user_ext');
            $table->string('requestor_fullname');
            $table->string('requestor_dept');
            $table->string('requestor_email');
            $table->string('requestor_ext');
            $table->string('request_type');
            $table->string('requirement_type');
            $table->string('application');
            $table->string('others');
            $table->string('description');
            $table->string('criticality');
            $table->string('required_completion_date');
            $table->string('required_owner_approval');
            $table->string('urf_status');
            $table->string('user_created');
            $table->timestamps();
            $table->string('user_lastmaintain');

        });


        Schema::create('ticket_it_log', function (Blueprint $table) {
            $table->id();
            $table->integer('urf_no');
            $table->string('urf_status');
            $table->string('remark');
            $table->string('user_created');
            $table->timestamps();
            $table->string('user_lastmaintain');

           
        });

        Schema::create('ticket_it_draft', function (Blueprint $table) {
            $table->id();
            $table->integer('urf_no');
            $table->string('user_fullname');
            $table->string('user_dept');
            $table->string('user_email');
            $table->string('user_ext');
            $table->string('requestor_fullname');
            $table->string('requestor_dept');
            $table->string('requestor_email');
            $table->string('requestor_ext');
            $table->string('request_type');
            $table->string('requirement_type');
            $table->string('application');
            $table->string('others');
            $table->string('description');
            $table->string('criticality');
            $table->string('required_completion_date');
            $table->string('required_owner_approval');
            $table->string('urf_status');
            $table->string('user_created');
            $table->timestamps();
            $table->string('user_lastmaintain');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_it');
        Schema::dropIfExists('ticket_it_log');
        Schema::dropIfExists('ticket_it_draft');
    }
}
