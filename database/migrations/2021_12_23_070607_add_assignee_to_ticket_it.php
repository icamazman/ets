<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAssigneeToTicketIt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_it', function (Blueprint $table) {
            $table->string('assignTo')->nullable();
            $table->timestamp('dtassign')->nullable();
            $table->string('officer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_it', function (Blueprint $table) {
            //
        });
    }
}
